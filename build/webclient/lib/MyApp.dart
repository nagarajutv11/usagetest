import 'main/components/LoginScreen.dart';
import 'package:flutter/material.dart';
import 'theme.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Test - Webclient',
        theme: ThemeData(
            brightness: Brightness.light,
            visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0)),
        home: Scaffold(body: LoginScreen()),
        debugShowCheckedModeBanner: false);
  }
}
