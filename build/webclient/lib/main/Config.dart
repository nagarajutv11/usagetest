import 'package:global_configuration/global_configuration.dart';

class Config {
  static Config _configObject;
  String _baseHttpUrl;
  String _baseWSurl;
  String _buildNumber;
  String _buildVesion;
  Config._init();
  factory Config.get() {
    if (_configObject == null) {
      _configObject = Config._init();
    }
    return _configObject;
  }
  load(GlobalConfiguration configuration) {
    _baseHttpUrl = configuration.getString('BASE_HTTP_URL');
    _baseWSurl = configuration.getString('BASE_WS_URL');
    _buildNumber = configuration.getString('BUILD_NUMBER');
    _buildVesion = configuration.getString('BUILD_VERSION');
  }

  String get baseHttpUrl {
    return _baseHttpUrl;
  }

  String get baseWSurl {
    return _baseWSurl;
  }

  String get buildNumber {
    return _buildNumber;
  }

  String get buildVesion {
    return _buildVesion;
  }
}
