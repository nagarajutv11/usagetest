import '../models/PropertyTestData.dart';
import '../models/StructTestData.dart';
import 'DataHelper.dart';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:ui_dart/core.dart';

class StructValues {
  StructValues();
  static Decoration decoration(List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    DataHelper.getString(data.data, field);

    return (BoxDecoration(
        color: StructValues.colorValues(data.data, field),
        image: StructValues.decorationImageValue(data.data, field),
        borderRadius: (StructValues.borderRadiusValue(data.data, field)
            as BorderRadiusGeometry)) as Decoration);
  }

  static DecorationImage decorationImageValue(
      List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    DataHelper.getString(data.data, field);

    return DecorationImage(
        colorFilter: StructValues.colorFilterValues(data.data, field),
        matchTextDirection: DataHelper.getBoolean(data.data, field));
  }

  static ColorFilter colorFilterValues(
      List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    DataHelper.getString(data.data, field);

    return ColorFilterExt.filterMode(color: null, blendMode: null);
  }

  static BorderRadius borderRadiusValue(
      List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    return BorderRadius.only(
        topLeft: StructValues.radiusValue(data.data, field),
        topRight: StructValues.radiusValue(data.data, field),
        bottomLeft: StructValues.radiusValue(data.data, field),
        bottomRight: StructValues.radiusValue(data.data, field));
  }

  static Color colorValues(List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    return ColorExt.fromARGB(
        red: DataHelper.getInteger(data.data, 'red'),
        green: DataHelper.getInteger(data.data, 'green'),
        blue: DataHelper.getInteger(data.data, 'blue'),
        alpha: DataHelper.getInteger(data.data, 'alpha'));
  }

  static Radius radiusValue(List<PropertyTestData> refData, String field) {
    StructTestData data = DataHelper.getStructData(refData, field);

    if (data == null) {
      return null;
    }

    return RadiusExt.elliptical(
        x: DataHelper.getDouble(data.data, field),
        y: DataHelper.getDouble(data.data, field));
  }
}
