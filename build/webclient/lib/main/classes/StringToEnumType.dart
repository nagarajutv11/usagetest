import 'package:flutter/material.dart';

class StringToEnumType {
  static Alignment stringToAlignment(String str) {
    switch (str) {
      case 'topLeft':
        {
          return Alignment.topLeft;
        }

      case 'topCenter':
        {
          return Alignment.topCenter;
        }

      case 'topRight':
        {
          return Alignment.topRight;
        }

      case 'centerLeft':
        {
          return Alignment.centerLeft;
        }

      case 'center':
        {
          return Alignment.center;
        }

      case 'centerRight':
        {
          return Alignment.centerRight;
        }

      case 'bottomLeft':
        {
          return Alignment.bottomLeft;
        }

      case 'bottomCenter':
        {
          return Alignment.bottomCenter;
        }

      case 'bottomRight':
        {
          return Alignment.bottomRight;
        }
      default:
        {
          return null;
        }
    }
  }

  static BlendMode stringToBlendMode(String str) {
    switch (str) {
      case 'clear':
        {
          return BlendMode.clear;
        }

      case 'src':
        {
          return BlendMode.src;
        }

      case 'dst':
        {
          return BlendMode.dst;
        }

      case 'srcOver':
        {
          return BlendMode.srcOver;
        }

      case 'dstOver':
        {
          return BlendMode.dstOver;
        }

      case 'srcIn':
        {
          return BlendMode.srcIn;
        }

      case 'dstIn':
        {
          return BlendMode.dstIn;
        }

      case 'srcOut':
        {
          return BlendMode.srcOut;
        }

      case 'dstOut':
        {
          return BlendMode.dstOut;
        }

      case 'srcATop':
        {
          return BlendMode.srcATop;
        }

      case 'dstATop':
        {
          return BlendMode.dstATop;
        }

      case 'xor':
        {
          return BlendMode.xor;
        }

      case 'plus':
        {
          return BlendMode.plus;
        }

      case 'modulate':
        {
          return BlendMode.modulate;
        }

      case 'screen':
        {
          return BlendMode.screen;
        }

      case 'overlay':
        {
          return BlendMode.overlay;
        }

      case 'darken':
        {
          return BlendMode.darken;
        }

      case 'lighten':
        {
          return BlendMode.lighten;
        }

      case 'colorDodge':
        {
          return BlendMode.colorDodge;
        }

      case 'colorBurn':
        {
          return BlendMode.colorBurn;
        }

      case 'hardLight':
        {
          return BlendMode.hardLight;
        }

      case 'softLight':
        {
          return BlendMode.softLight;
        }

      case 'difference':
        {
          return BlendMode.difference;
        }

      case 'exclusion':
        {
          return BlendMode.exclusion;
        }

      case 'multiply':
        {
          return BlendMode.multiply;
        }

      case 'hue':
        {
          return BlendMode.hue;
        }

      case 'saturation':
        {
          return BlendMode.saturation;
        }

      case 'color':
        {
          return BlendMode.color;
        }

      case 'luminosity':
        {
          return BlendMode.luminosity;
        }
      default:
        {
          return null;
        }
    }
  }

  static BorderStyle stringToBorderStyle(String str) {
    switch (str) {
      case 'none':
        {
          return BorderStyle.none;
        }

      case 'solid':
        {
          return BorderStyle.solid;
        }
      default:
        {
          return null;
        }
    }
  }

  static BoxFit stringToBoxFit(String str) {
    switch (str) {
      case 'fill':
        {
          return BoxFit.fill;
        }

      case 'contain':
        {
          return BoxFit.contain;
        }

      case 'cover':
        {
          return BoxFit.cover;
        }

      case 'fitWidth':
        {
          return BoxFit.fitWidth;
        }

      case 'fitHeight':
        {
          return BoxFit.fitHeight;
        }

      case 'none':
        {
          return BoxFit.none;
        }

      case 'scaleDown':
        {
          return BoxFit.scaleDown;
        }
      default:
        {
          return null;
        }
    }
  }

  static BoxShape stringToBoxShape(String str) {
    switch (str) {
      case 'rectangle':
        {
          return BoxShape.rectangle;
        }

      case 'circle':
        {
          return BoxShape.circle;
        }
      default:
        {
          return null;
        }
    }
  }

  static ImageRepeat stringToImageRepeat(String str) {
    switch (str) {
      case 'repeat':
        {
          return ImageRepeat.repeat;
        }

      case 'repeatX':
        {
          return ImageRepeat.repeatX;
        }

      case 'repeatY':
        {
          return ImageRepeat.repeatY;
        }

      case 'noRepeat':
        {
          return ImageRepeat.noRepeat;
        }
      default:
        {
          return null;
        }
    }
  }
}
