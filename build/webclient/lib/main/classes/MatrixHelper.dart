import 'dart:ui';
import 'package:flutter/material.dart';

class MatrixHelper {
  MatrixHelper();
  static Matrix4 getMatrixData(Offset offSet, double data) {
    return Matrix4.identity()
      ..translate(offSet.dx, offSet.dy)
      ..scale(data);
  }
}
