import 'package:graphql/internal.dart';
import '../utils/ReferenceCatch.dart';
import 'Subscription.dart';
import 'dart:async';

class ObjectListSubscription {
  static ObjectListSubscription _object;
  Subscription subscription;
  ReferenceCatch _referenceCatch;
  Map<Object, StreamSubscription> subscriptions;
  factory ObjectListSubscription.get() {
    if (_object == null) {
      _object = ObjectListSubscription._init();
    }

    return _object;
  }
  ObjectListSubscription._init() {
    this.subscription = Subscription.get();

    this.subscriptions = Map<Object, StreamSubscription>();

    this._referenceCatch = ReferenceCatch.get();
  }
  void unsubscribe(Object obj) {
    StreamSubscription sub = subscriptions[obj];

    if (sub != null) {
      sub.cancel();
    }
  }
}
