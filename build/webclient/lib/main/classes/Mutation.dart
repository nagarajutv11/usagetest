import '../utils/GraphQLClientInit.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../utils/ReferenceCatch.dart';

class Mutation {
  GraphQLClient _client;
  static Mutation _mutationObject;
  ReferenceCatch _referenceCatch;
  Mutation._init() {
    this._client = GraphQLClientInit.get();

    this._referenceCatch = ReferenceCatch.get();
  }
  factory Mutation.get() {
    if (_mutationObject == null) {
      _mutationObject = Mutation._init();
    }

    return _mutationObject;
  }
}
