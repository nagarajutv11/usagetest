import 'dart:async';

import 'package:d3e_lang_dart/core.dart';
import 'package:d3e_studio/main/Config.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphql/client.dart';
import 'package:graphql/internal.dart';
import '../utils/GraphQLClientInit.dart';
import '../utils/BaseObject.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/ReferenceCatch.dart';
import '../utils/SubscriptionHolder.dart';
import '../utils/SubscriptionQuery.dart';

typedef OnEvent(String type, DBObject obj);

class Subscription {
  GraphQLClient _client;
  static Subscription _subscriptionObject;
  ReferenceCatch _referenceCatch;
  Map<String, SubscriptionHolder> _subscriptions = Map();

  Subscription._init() {
    this._referenceCatch = ReferenceCatch.get();
  }
  GraphQLClient client() {
    if (_client == null) {
      String token = GraphQLClientInit.token;
      final _link = WebSocketLink(
          url: Config.get().baseWSurl + '/subscriptions?token=$token',
          config: SocketClientConfig(queryAndMutationTimeout: null));
      _client = GraphQLClient(link: _link, cache: DummyCache());
    }
    return _client;
  }

  void disconnect() {
    if (_client != null) {
      (_client.link as WebSocketLink).dispose();
      _client = null;
    }
  }

  factory Subscription.get() {
    if (_subscriptionObject == null) {
      _subscriptionObject = Subscription._init();
    }

    return _subscriptionObject;
  }

  Stream<FetchResult> subscribeQuery(
      String query, Map<String, dynamic> variables) {
    Operation operation = Operation(document: query, variables: variables);
    Stream<FetchResult> stream = client().subscribe(operation);
    return stream;
  }

  bool subscribeById(String type, List<int> ids, SubscriptionQuery query) {
    SubscriptionHolder holder = getHolder(type);
    holder.addQuery(query, ids);
    resubscribeById(holder);
    return true;
  }

  void resubscribeById(SubscriptionHolder holder) {
    holder.subscription?.cancel();
    holder.prepareQuery(true);
    if (holder.query == null || holder.query == '') {
      return;
    }
    subscribeInternal(holder, 'on${holder.type}ChangeEventById',
        holder.getAllIds(), 'ids', '[Long]', holder.type, (t, o) {
      if (t == 'Delete') {
        holder.subscription?.cancel();
      }
    });
  }

  bool unsubscribeById(String type, List<int> ids, SubscriptionQuery fields) {
    SubscriptionHolder holder = getHolder(type);
    holder.remove(fields, ids);
    holder.prepareQuery(true);
    resubscribeById(holder);
    return true;
  }

  bool subscribeInverse(
      BaseObject parent,
      String subscriptionName,
      String field,
      String type,
      SubscriptionQuery query,
      BiConsumer<String, DBObject> onEvent) {
    SubscriptionHolder parentHolder = getHolder(parent.d3eType);
    int parentId = (parent as DBObject).id;
    SubscriptionHolder holder =
        parentHolder.getInverseHolder(field + '-' + parentId.toString());
    holder.addQuery(query, [parentId]);
    bool modified = holder.prepareQuery(false);
    if (modified) {
      subscribeInternal(
          holder, subscriptionName, parentId, 'id', 'Long', type, onEvent);
    }
    return modified;
  }

  bool unsubscribeInverse(
      BaseObject parent, String field, SubscriptionQuery query) {
    SubscriptionHolder parentHolder = getHolder(parent.d3eType);
    int parentId = (parent as DBObject).id;
    SubscriptionHolder holder =
        parentHolder.getInverseHolder(field + '-' + parentId.toString());
    holder.remove(query, [parentId]);
    bool modified = holder.prepareQuery(false);
    if (modified) {
      holder.subscription?.cancel();
    }
    parentHolder.removeInverseHolder(field + '-' + parentId.toString());
    return modified;
  }

  void subscribeInternal(
      SubscriptionHolder holder,
      String subscriptionName,
      dynamic input,
      String inputName,
      String inputType,
      String type,
      BiConsumer<String, DBObject> onEvent) {
    String subscriptionQuery = r'subscription($input: ' +
        inputType +
        ') {' +
        subscriptionName +
        '(' +
        inputName +
        r': $input) { model ' +
        holder.query +
        ' changeType }}';
    Map<String, dynamic> variables = Map<String, dynamic>();
    variables['input'] = input;
    Stream<FetchResult> stream = subscribeQuery(subscriptionQuery, variables);
    if (stream == null) {
      return;
    }
    try {
      StreamSubscription newSub = stream.listen(
        (data) {
          dynamic res = data.data[subscriptionName];
          String changeType = res['changeType'];
          ReaderContext ctx = ReaderContext(res, _referenceCatch);
          DBObject obj = ctx.readRefStart('model', type);
          if (changeType == 'Delete') {
            obj.saveStatus = DBSaveStatus.Deleted;
          }
          onEvent(changeType, obj);
        },
        onError: (e) => {},
        onDone: () => {},
      );
      holder.subscription?.cancel();
      holder.subscription = newSub;
    } catch (e) {
      print(e.toString());
    }
  }

  SubscriptionHolder getHolder(String type) {
    SubscriptionHolder holder = _subscriptions[type];

    if (holder == null) {
      holder = SubscriptionHolder(type);

      _subscriptions[type] = holder;
    }

    return holder;
  }
}
