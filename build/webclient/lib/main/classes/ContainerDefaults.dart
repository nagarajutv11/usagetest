import '../models/CRef.dart';
import '../models/CRefData.dart';
import '../models/PropertyValue.dart';
import 'CanvasUtils.dart';
import 'StructValues.dart';
import 'package:flutter/material.dart';

class ContainerDefaults {
  ContainerDefaults();
  static Decoration decorationValue(CRef cRef, CRefData data) {
    PropertyValue pv = CanvasUtils.checkPropisAvail(cRef, 'decoration');

    if (pv != null) {
      if (true) {
        return StructValues.decoration(data.data, 'decoration');
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
