import '../models/PropertyTestData.dart';
import '../models/StructTestData.dart';

class DataHelper {
  DataHelper();
  static String getString(List<PropertyTestData> data, String field) {
    PropertyTestData propValue = DataHelper._getPropValue(data, field);

    if (propValue == null || propValue.primitiveValue.isEmpty) {
      return '';
    }

    return propValue.primitiveValue.first;
  }

  static PropertyTestData _getPropValue(
      List<PropertyTestData> data, String field) {
    return data.singleWhere((i) => i.identity == field, orElse: () => null);
  }

  static StructTestData getStructData(
      List<PropertyTestData> data, String identity) {
    PropertyTestData ptd =
        data.singleWhere((i) => i.identity == identity, orElse: () => null);

    if (ptd == null || ptd.structValue.isEmpty) {
      return null;
    }

    return ptd.structValue.first;
  }

  static int getInteger(List<PropertyTestData> data, String field) {
    PropertyTestData propValue = DataHelper._getPropValue(data, field);

    if (propValue == null || propValue.primitiveValue.isEmpty) {
      return 0;
    }

    return int.parse(propValue.primitiveValue.first);
  }

  static double getDouble(List<PropertyTestData> data, String field) {
    PropertyTestData propValue = DataHelper._getPropValue(data, field);

    if (propValue == null || propValue.primitiveValue.isEmpty) {
      return 0.0;
    }

    return double.parse(propValue.primitiveValue.first);
  }

  static bool getBoolean(List<PropertyTestData> data, String field) {
    PropertyTestData propValue = DataHelper._getPropValue(data, field);

    if (propValue == null || propValue.primitiveValue.isEmpty) {
      return false;
    }

    return propValue.primitiveValue.first == 'true';
  }
}
