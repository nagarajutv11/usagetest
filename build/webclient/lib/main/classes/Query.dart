import 'LoginResult.dart';
import 'package:graphql_flutter/graphql_flutter.dart' hide Subscription;
import '../models/User.dart';
import '../utils/GraphQLClientInit.dart';
import '../utils/LocalDataStore.dart';
import '../utils/ReferenceCatch.dart';
import 'Subscription.dart';

class Query {
  GraphQLClient _client;
  static Query _queryObject;
  ReferenceCatch _referenceCatch;
  Query._init() {
    this._client = GraphQLClientInit.get();

    this._referenceCatch = ReferenceCatch.get();
  }
  factory Query.get() {
    if (_queryObject == null) {
      _queryObject = Query._init();
    }

    return _queryObject;
  }
  Future<User> currentUser() async {
    GraphQLClientInit.token = (await LocalDataStore.get().getToken());

    return (await LocalDataStore.get().currentUser());
  }

  void logout() async {
    LocalDataStore.get().setUser(null, null);

    Subscription.get().disconnect();
  }
}
