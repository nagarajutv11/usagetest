import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'PropertyValue.dart';

class CRef extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  List<PropertyValue> _data = [];
  List<CloneContext> _save_points = List();
  CRef({List<PropertyValue> data}) {
    this.setData(data ?? []);
  }
  String get d3eType {
    return 'CRef';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  List<PropertyValue> get data {
    return _data;
  }

  void setData(List<PropertyValue> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterCRef = null);
    }

    if (val != null) {
      for (PropertyValue o in val) {
        o.masterCRef = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyValue val, [int index = -1]) {
    val.masterCRef = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyValue val) {
    _data.remove(val);

    val.masterCRef = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);
  }

  CRef deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    CRef obj = (dbObj as CRef);

    obj._id = _id;

    ctx.cloneChildList(_data, (v) => obj.setData(v));
  }
}

class CRefReader<T extends CRef> implements JsonReader<T> {
  T createNewInstance(int id) {
    CRef obj = CRef();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('data')) {
      ctx.readRefList<PropertyValue>(
          'data', 'PropertyValue', obj._data, (val) => obj.setData(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyValue>(obj._data);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
