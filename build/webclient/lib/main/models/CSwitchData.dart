import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CIfData.dart';
import 'CNodeData.dart';
import 'CPopupData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'ComponentTestData.dart';
import 'PropertyTestData.dart';

class CSwitchData extends CNodeData implements DBObject {
  int _index = 0;
  PropertyTestData _onValue;
  CNodeData _caseData;
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CSwitchData(
      {CNodeData caseData,
      double height,
      String identity,
      int index,
      PropertyTestData onValue,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setCaseData(caseData ?? null);

    this.setIndex(index ?? 0);

    this.setOnValue(onValue ?? null);
  }
  String get d3eType {
    return 'CSwitchData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  int get index {
    return _index;
  }

  void setIndex(int val) {
    bool isValChanged = _index != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('index');

    _index = val;

    fire('index');
  }

  PropertyTestData get onValue {
    return _onValue;
  }

  void setOnValue(PropertyTestData val) {
    bool isValChanged = _onValue != val;

    if (!isValChanged) {
      return;
    }

    if (_onValue != null) {
      _onValue.masterCSwitchData = null;
    }

    if (val != null) {
      val.masterCSwitchData = this;

      val.childPropertyInMaster = 'onValue';
    }

    this.updateD3EChanges('onValue');

    updateObservable('onValue', _onValue, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _onValue = val;

    fire('onValue');
  }

  CNodeData get caseData {
    return _caseData;
  }

  void setCaseData(CNodeData val) {
    bool isValChanged = _caseData != val;

    if (!isValChanged) {
      return;
    }

    if (_caseData != null) {
      _caseData.masterCSwitchData = null;
    }

    if (val != null) {
      val.masterCSwitchData = this;

      val.childPropertyInMaster = 'caseData';
    }

    this.updateD3EChanges('caseData');

    updateObservable('caseData', _caseData, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _caseData = val;

    fire('caseData');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CSwitchData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CSwitchData obj = (dbObj as CSwitchData);

    obj.setIndex(_index);

    ctx.cloneChild(_onValue, (v) => obj.setOnValue(v));

    ctx.cloneChild(_caseData, (v) => obj.setCaseData(v));
  }
}

class CSwitchDataReader<T extends CSwitchData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CSwitchData obj = CSwitchData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('index')) {
      obj.setIndex(ctx.readInteger('index'));
    }

    if (ctx.has('onValue')) {
      ctx.readRef<PropertyTestData>('onValue', 'PropertyTestData', obj._onValue,
          (val) => obj.setOnValue(val));
    }

    if (ctx.has('caseData')) {
      ctx.readRef<CNodeData>('caseData', 'CNodeData', obj._caseData,
          (val) => obj.setCaseData(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('index')) {
      jsonMap['index'] = obj._index;
    }

    if (obj.d3eChanges.contains('onValue')) {
      jsonMap['onValue'] = ctx.writeObj<PropertyTestData>(obj._onValue);
    }

    if (obj.d3eChanges.contains('caseData')) {
      jsonMap['caseData'] = ctx.writeObjUnion<CNodeData>(obj._caseData);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
