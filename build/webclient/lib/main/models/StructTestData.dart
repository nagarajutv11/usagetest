import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'ComponentTestData.dart';
import 'PropertyTestData.dart';

class StructTestData extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _identity = '';
  List<PropertyTestData> _data = [];
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  List<CloneContext> _save_points = List();
  StructTestData({List<PropertyTestData> data, String identity}) {
    this.setData(data ?? []);

    this.setIdentity(identity ?? '');
  }
  String get d3eType {
    return 'StructTestData';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get identity {
    return _identity;
  }

  void setIdentity(String val) {
    bool isValChanged = _identity != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('identity');

    _identity = val;

    fire('identity');
  }

  List<PropertyTestData> get data {
    return _data;
  }

  void setData(List<PropertyTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterStructTestData = null);
    }

    if (val != null) {
      for (PropertyTestData o in val) {
        o.masterStructTestData = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    {
      for (PropertyTestData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyTestData val, [int index = -1]) {
    val.masterStructTestData = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyTestData val) {
    _data.remove(val);

    val.masterStructTestData = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);
  }

  StructTestData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    StructTestData obj = (dbObj as StructTestData);

    obj._id = _id;

    obj.setIdentity(_identity);

    ctx.cloneChildList(_data, (v) => obj.setData(v));
  }
}

class StructTestDataReader<T extends StructTestData> implements JsonReader<T> {
  T createNewInstance(int id) {
    StructTestData obj = StructTestData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('identity')) {
      obj.setIdentity(ctx.readString('identity'));
    }

    if (ctx.has('data')) {
      ctx.readRefList<PropertyTestData>(
          'data', 'PropertyTestData', obj._data, (val) => obj.setData(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('identity')) {
      jsonMap['identity'] = obj._identity;
    }

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyTestData>(obj._data);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
