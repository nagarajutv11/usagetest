import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CNodeData.dart';
import 'CPopupData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';

class CIfData extends CNodeData implements DBObject {
  bool _condition = false;
  CNodeData _value;
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CIfData(
      {bool condition,
      double height,
      String identity,
      CNodeData value,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setCondition(condition ?? false);

    this.setValue(value ?? null);
  }
  String get d3eType {
    return 'CIfData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  bool get condition {
    return _condition;
  }

  void setCondition(bool val) {
    bool isValChanged = _condition != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('condition');

    _condition = val;

    fire('condition');
  }

  CNodeData get value {
    return _value;
  }

  void setValue(CNodeData val) {
    bool isValChanged = _value != val;

    if (!isValChanged) {
      return;
    }

    if (_value != null) {
      _value.masterCIfData = null;
    }

    if (val != null) {
      val.masterCIfData = this;

      val.childPropertyInMaster = 'value';
    }

    this.updateD3EChanges('value');

    updateObservable('value', _value, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _value = val;

    fire('value');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CIfData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CIfData obj = (dbObj as CIfData);

    obj.setCondition(_condition);

    ctx.cloneChild(_value, (v) => obj.setValue(v));
  }
}

class CIfDataReader<T extends CIfData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CIfData obj = CIfData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('condition')) {
      obj.setCondition(ctx.readBoolean('condition'));
    }

    if (ctx.has('value')) {
      ctx.readRef<CNodeData>(
          'value', 'CNodeData', obj._value, (val) => obj.setValue(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('condition')) {
      jsonMap['condition'] = obj._condition;
    }

    if (obj.d3eChanges.contains('value')) {
      jsonMap['value'] = ctx.writeObjUnion<CNodeData>(obj._value);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
