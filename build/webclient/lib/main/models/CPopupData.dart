import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CIfData.dart';
import 'CNodeData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';

class CPopupData extends CNodeData implements DBObject {
  bool _showOn = false;
  CNodeData _body;
  CNodeData _placeHolder;
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CPopupData(
      {CNodeData body,
      double height,
      String identity,
      CNodeData placeHolder,
      bool showOn,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setBody(body ?? null);

    this.setPlaceHolder(placeHolder ?? null);

    this.setShowOn(showOn ?? false);
  }
  String get d3eType {
    return 'CPopupData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  bool get showOn {
    return _showOn;
  }

  void setShowOn(bool val) {
    bool isValChanged = _showOn != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('showOn');

    _showOn = val;

    fire('showOn');
  }

  CNodeData get body {
    return _body;
  }

  void setBody(CNodeData val) {
    bool isValChanged = _body != val;

    if (!isValChanged) {
      return;
    }

    if (_body != null) {
      _body.masterCPopupData = null;
    }

    if (val != null) {
      val.masterCPopupData = this;

      val.childPropertyInMaster = 'body';
    }

    this.updateD3EChanges('body');

    updateObservable('body', _body, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _body = val;

    fire('body');
  }

  CNodeData get placeHolder {
    return _placeHolder;
  }

  void setPlaceHolder(CNodeData val) {
    bool isValChanged = _placeHolder != val;

    if (!isValChanged) {
      return;
    }

    if (_placeHolder != null) {
      _placeHolder.masterCPopupData = null;
    }

    if (val != null) {
      val.masterCPopupData = this;

      val.childPropertyInMaster = 'placeHolder';
    }

    this.updateD3EChanges('placeHolder');

    updateObservable('placeHolder', _placeHolder, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _placeHolder = val;

    fire('placeHolder');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CPopupData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CPopupData obj = (dbObj as CPopupData);

    obj.setShowOn(_showOn);

    ctx.cloneChild(_body, (v) => obj.setBody(v));

    ctx.cloneChild(_placeHolder, (v) => obj.setPlaceHolder(v));
  }
}

class CPopupDataReader<T extends CPopupData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CPopupData obj = CPopupData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('showOn')) {
      obj.setShowOn(ctx.readBoolean('showOn'));
    }

    if (ctx.has('body')) {
      ctx.readRef<CNodeData>(
          'body', 'CNodeData', obj._body, (val) => obj.setBody(val));
    }

    if (ctx.has('placeHolder')) {
      ctx.readRef<CNodeData>('placeHolder', 'CNodeData', obj._placeHolder,
          (val) => obj.setPlaceHolder(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('showOn')) {
      jsonMap['showOn'] = obj._showOn;
    }

    if (obj.d3eChanges.contains('body')) {
      jsonMap['body'] = ctx.writeObjUnion<CNodeData>(obj._body);
    }

    if (obj.d3eChanges.contains('placeHolder')) {
      jsonMap['placeHolder'] = ctx.writeObjUnion<CNodeData>(obj._placeHolder);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
