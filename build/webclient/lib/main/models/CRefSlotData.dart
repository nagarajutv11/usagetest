import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CNodeData.dart';
import 'CRefData.dart';
import 'CSlotData.dart';
import 'ComponentTestData.dart';

class CRefSlotData extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _slot = '';
  CNodeData _child;
  List<CNodeData> _children = [];
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  CRefData _masterCRefData;
  CSlotData _masterCSlotData;
  List<CloneContext> _save_points = List();
  CRefSlotData({CNodeData child, List<CNodeData> children, String slot}) {
    this.setChild(child ?? null);

    this.setChildren(children ?? []);

    this.setSlot(slot ?? '');
  }
  String get d3eType {
    return 'CRefSlotData';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get slot {
    return _slot;
  }

  void setSlot(String val) {
    bool isValChanged = _slot != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('slot');

    _slot = val;

    fire('slot');
  }

  CNodeData get child {
    return _child;
  }

  void setChild(CNodeData val) {
    bool isValChanged = _child != val;

    if (!isValChanged) {
      return;
    }

    if (_child != null) {
      _child.masterCRefSlotData = null;
    }

    if (val != null) {
      val.masterCRefSlotData = this;

      val.childPropertyInMaster = 'child';
    }

    this.updateD3EChanges('child');

    updateObservable('child', _child, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _child = val;

    fire('child');
  }

  List<CNodeData> get children {
    return _children;
  }

  void setChildren(List<CNodeData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_children, val);

    if (!isValChanged) {
      return;
    }

    if (_children != null) {
      _children.forEach((one) => one.masterCRefSlotData = null);
    }

    if (val != null) {
      for (CNodeData o in val) {
        o.masterCRefSlotData = this;

        o.childPropertyInMaster = 'children';
      }
    }

    this.updateD3EChanges('children');

    updateObservableColl('children', _children, val);

    {
      for (CNodeData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _children.clear();

    _children.addAll(val);

    fire('children');
  }

  void addToChildren(CNodeData val, [int index = -1]) {
    val.masterCRefSlotData = this;

    val.childPropertyInMaster = 'children';

    if (index == -1) {
      if (!children.contains(val)) _children.add(val);
    } else {
      _children.insert(index, val);
    }

    fire('children');

    updateObservable('children', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('children');
  }

  void removeFromChildren(CNodeData val) {
    _children.remove(val);

    val.masterCRefSlotData = null;

    fire('children');

    removeObservable('children', val);

    this.updateD3EChanges('children');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CSlotData get masterCSlotData {
    return _masterCSlotData;
  }

  set masterCSlotData(CSlotData val) {
    _masterCSlotData = val;
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSlotData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CRefSlotData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    CRefSlotData obj = (dbObj as CRefSlotData);

    obj._id = _id;

    obj.setSlot(_slot);

    ctx.cloneChild(_child, (v) => obj.setChild(v));

    ctx.cloneChildList(_children, (v) => obj.setChildren(v));
  }
}

class CRefSlotDataReader<T extends CRefSlotData> implements JsonReader<T> {
  T createNewInstance(int id) {
    CRefSlotData obj = CRefSlotData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('slot')) {
      obj.setSlot(ctx.readString('slot'));
    }

    if (ctx.has('child')) {
      ctx.readRef<CNodeData>(
          'child', 'CNodeData', obj._child, (val) => obj.setChild(val));
    }

    if (ctx.has('children')) {
      ctx.readRefList<CNodeData>('children', 'CNodeData', obj._children,
          (val) => obj.setChildren(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('slot')) {
      jsonMap['slot'] = obj._slot;
    }

    if (obj.d3eChanges.contains('child')) {
      jsonMap['child'] = ctx.writeObjUnion<CNodeData>(obj._child);
    }

    if (obj.d3eChanges.contains('children')) {
      jsonMap['children'] = ctx.writeObjUnionList<CNodeData>(obj._children);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
