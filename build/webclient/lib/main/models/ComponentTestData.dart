import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CNodeData.dart';
import 'PropertyTestData.dart';
import 'StructTestData.dart';

class ComponentTestData extends ObjectObservable implements DBObject {
  static String D3EFullQuery =
      '{ id identity data{ id identity primitiveValue modelValue{ id } structValue{ id } } structTempData{ id identity data{ id identity primitiveValue modelValue{ id } structValue{ id } } } buildData flatBuildData{ __typename ... on CForData { id { id identity x y width height data{ id identity primitiveValue modelValue{ id } structValue{ id } } itemsFlat } }... on CIfData { id { id identity x y width height condition valueFlat } }... on CPopupData { id { id identity x y width height showOn bodyFlat placeHolderFlat } }... on CRefData { id { id identity x y width height data{ id identity primitiveValue modelValue{ id } structValue{ id } } builderFlat buildDataFlat childFlat childrenFlat slots flatSlots{ id slot childFlat childrenFlat } } }... on CSlotData { id { id identity x y width height slot actualFlat } }... on CSwitchData { id { id identity x y width height index onValue{ id identity primitiveValue modelValue{ id } structValue{ id } } caseDataFlat } }} }';
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _identity = '';
  List<PropertyTestData> _data = [];
  List<StructTestData> _structTempData = [];
  CNodeData _buildData;
  Set<CNodeData> _flatBuildData = {};
  List<CloneContext> _save_points = List();
  ComponentTestData(
      {CNodeData buildData,
      List<PropertyTestData> data,
      String identity,
      List<StructTestData> structTempData}) {
    this.setBuildData(buildData ?? null);

    this.setData(data ?? []);

    this.setIdentity(identity ?? '');

    this.setStructTempData(structTempData ?? []);
  }
  String get d3eType {
    return 'ComponentTestData';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get identity {
    return _identity;
  }

  void setIdentity(String val) {
    bool isValChanged = _identity != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('identity');

    _identity = val;

    fire('identity');
  }

  List<PropertyTestData> get data {
    return _data;
  }

  void setData(List<PropertyTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterComponentTestData = null);
    }

    if (val != null) {
      for (PropertyTestData o in val) {
        o.masterComponentTestData = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    {
      for (PropertyTestData o in val) {
        o.setInValue(this);
      }
    }

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyTestData val, [int index = -1]) {
    val.masterComponentTestData = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    {
      val.setInValue(this);
    }

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyTestData val) {
    _data.remove(val);

    val.masterComponentTestData = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  List<StructTestData> get structTempData {
    return _structTempData;
  }

  void setStructTempData(List<StructTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_structTempData, val);

    if (!isValChanged) {
      return;
    }

    if (_structTempData != null) {
      _structTempData.forEach((one) => one.masterComponentTestData = null);
    }

    if (val != null) {
      for (StructTestData o in val) {
        o.masterComponentTestData = this;

        o.childPropertyInMaster = 'structTempData';
      }
    }

    this.updateD3EChanges('structTempData');

    updateObservableColl('structTempData', _structTempData, val);

    {
      for (StructTestData o in val) {
        o.setInValue(this);
      }
    }

    _structTempData.clear();

    _structTempData.addAll(val);

    fire('structTempData');
  }

  void addToStructTempData(StructTestData val, [int index = -1]) {
    val.masterComponentTestData = this;

    val.childPropertyInMaster = 'structTempData';

    if (index == -1) {
      if (!structTempData.contains(val)) _structTempData.add(val);
    } else {
      _structTempData.insert(index, val);
    }

    fire('structTempData');

    updateObservable('structTempData', null, val);

    {
      val.setInValue(this);
    }

    this.updateD3EChanges('structTempData');
  }

  void removeFromStructTempData(StructTestData val) {
    _structTempData.remove(val);

    val.masterComponentTestData = null;

    fire('structTempData');

    removeObservable('structTempData', val);

    this.updateD3EChanges('structTempData');
  }

  CNodeData get buildData {
    return _buildData;
  }

  void setBuildData(CNodeData val) {
    bool isValChanged = _buildData != val;

    if (!isValChanged) {
      return;
    }

    if (_buildData != null) {
      _buildData.masterComponentTestData = null;
    }

    if (val != null) {
      val.masterComponentTestData = this;

      val.childPropertyInMaster = 'buildData';
    }

    this.updateD3EChanges('buildData');

    updateObservable('buildData', _buildData, val);

    {
      if (val != null) {
        val.setInValue(this);
      }
    }

    _buildData = val;

    fire('buildData');
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);
  }

  ComponentTestData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    ComponentTestData obj = (dbObj as ComponentTestData);

    obj._id = _id;

    obj.setIdentity(_identity);

    ctx.cloneChildList(_data, (v) => obj.setData(v));

    ctx.cloneChildList(_structTempData, (v) => obj.setStructTempData(v));

    ctx.cloneChild(_buildData, (v) => obj.setBuildData(v));
  }
}

class ComponentTestDataReader<T extends ComponentTestData>
    implements JsonReader<T> {
  T createNewInstance(int id) {
    ComponentTestData obj = ComponentTestData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('identity')) {
      obj.setIdentity(ctx.readString('identity'));
    }

    if (ctx.has('data')) {
      ctx.readRefList<PropertyTestData>(
          'data', 'PropertyTestData', obj._data, (val) => obj.setData(val));
    }

    if (ctx.has('structTempData')) {
      ctx.readRefList<StructTestData>('structTempData', 'StructTestData',
          obj._structTempData, (val) => obj.setStructTempData(val));
    }

    if (ctx.has('buildData')) {
      ctx.readRef<CNodeData>('buildData', 'CNodeData', obj._buildData,
          (val) => obj.setBuildData(val));
    }

    if (ctx.has('flatBuildData')) {
      ctx.readRefSet<CNodeData>('flatBuildData', 'CNodeData',
          obj._flatBuildData, (val) => obj._flatBuildData = val);
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('identity')) {
      jsonMap['identity'] = obj._identity;
    }

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyTestData>(obj._data);
    }

    if (obj.d3eChanges.contains('structTempData')) {
      jsonMap['structTempData'] =
          ctx.writeObjList<StructTestData>(obj._structTempData);
    }

    if (obj.d3eChanges.contains('buildData')) {
      jsonMap['buildData'] = ctx.writeObjUnion<CNodeData>(obj._buildData);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
