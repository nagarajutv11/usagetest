import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CRef.dart';

class PropertyValue extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _thenValue = '';
  String _elseValue = '';
  String _defaultValue = '';
  String _value = '';
  String _fromTheme = '';
  String _childPropertyInMaster = '';
  CRef _masterCRef;
  List<CloneContext> _save_points = List();
  PropertyValue(
      {String defaultValue,
      String elseValue,
      String fromTheme,
      String thenValue,
      String value}) {
    this.setDefaultValue(defaultValue ?? '');

    this.setElseValue(elseValue ?? '');

    this.setFromTheme(fromTheme ?? '');

    this.setThenValue(thenValue ?? '');

    this.setValue(value ?? '');
  }
  String get d3eType {
    return 'PropertyValue';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get thenValue {
    return _thenValue;
  }

  void setThenValue(String val) {
    bool isValChanged = _thenValue != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('thenValue');

    _thenValue = val;

    fire('thenValue');
  }

  String get elseValue {
    return _elseValue;
  }

  void setElseValue(String val) {
    bool isValChanged = _elseValue != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('elseValue');

    _elseValue = val;

    fire('elseValue');
  }

  String get defaultValue {
    return _defaultValue;
  }

  void setDefaultValue(String val) {
    bool isValChanged = _defaultValue != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('defaultValue');

    _defaultValue = val;

    fire('defaultValue');
  }

  String get value {
    return _value;
  }

  void setValue(String val) {
    bool isValChanged = _value != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('value');

    _value = val;

    fire('value');
  }

  String get fromTheme {
    return _fromTheme;
  }

  void setFromTheme(String val) {
    bool isValChanged = _fromTheme != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('fromTheme');

    _fromTheme = val;

    fire('fromTheme');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  CRef get masterCRef {
    return _masterCRef;
  }

  set masterCRef(CRef val) {
    _masterCRef = val;
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);

    this._masterCRef?.updateD3EChanges(this.childPropertyInMaster);
  }

  PropertyValue deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    PropertyValue obj = (dbObj as PropertyValue);

    obj._id = _id;

    obj.setThenValue(_thenValue);

    obj.setElseValue(_elseValue);

    obj.setDefaultValue(_defaultValue);

    obj.setValue(_value);

    obj.setFromTheme(_fromTheme);
  }
}

class PropertyValueReader<T extends PropertyValue> implements JsonReader<T> {
  T createNewInstance(int id) {
    PropertyValue obj = PropertyValue();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('thenValue')) {
      obj.setThenValue(ctx.readString('thenValue'));
    }

    if (ctx.has('elseValue')) {
      obj.setElseValue(ctx.readString('elseValue'));
    }

    if (ctx.has('defaultValue')) {
      obj.setDefaultValue(ctx.readString('defaultValue'));
    }

    if (ctx.has('value')) {
      obj.setValue(ctx.readString('value'));
    }

    if (ctx.has('fromTheme')) {
      obj.setFromTheme(ctx.readString('fromTheme'));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('thenValue')) {
      jsonMap['thenValue'] = obj._thenValue;
    }

    if (obj.d3eChanges.contains('elseValue')) {
      jsonMap['elseValue'] = obj._elseValue;
    }

    if (obj.d3eChanges.contains('defaultValue')) {
      jsonMap['defaultValue'] = obj._defaultValue;
    }

    if (obj.d3eChanges.contains('value')) {
      jsonMap['value'] = obj._value;
    }

    if (obj.d3eChanges.contains('fromTheme')) {
      jsonMap['fromTheme'] = obj._fromTheme;
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
