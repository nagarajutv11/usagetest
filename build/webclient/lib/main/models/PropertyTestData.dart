import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CRefData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';
import 'ModelTestData.dart';
import 'StructTestData.dart';

class PropertyTestData extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _identity = '';
  List<String> _primitiveValue = [];
  List<ModelTestData> _modelValue = [];
  List<StructTestData> _structValue = [];
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ModelTestData _masterModelTestData;
  ComponentTestData _masterComponentTestData;
  StructTestData _masterStructTestData;
  CForData _masterCForData;
  CRefData _masterCRefData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  PropertyTestData(
      {String identity,
      List<ModelTestData> modelValue,
      List<String> primitiveValue,
      List<StructTestData> structValue}) {
    this.setIdentity(identity ?? '');

    this.setModelValue(modelValue ?? []);

    this.setPrimitiveValue(primitiveValue ?? []);

    this.setStructValue(structValue ?? []);
  }
  String get d3eType {
    return 'PropertyTestData';
  }

  void subscribe(SubscriptionContext ctx) {
    if (ctx.has('modelValue')) {
      ctx.performCollection('modelValue', _modelValue);
    }
  }

  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get identity {
    return _identity;
  }

  void setIdentity(String val) {
    bool isValChanged = _identity != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('identity');

    _identity = val;

    fire('identity');
  }

  List<String> get primitiveValue {
    return _primitiveValue;
  }

  void setPrimitiveValue(List<String> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_primitiveValue, val);

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('primitiveValue');

    _primitiveValue.clear();

    _primitiveValue.addAll(val);

    fire('primitiveValue');
  }

  void addToPrimitiveValue(String val, [int index = -1]) {
    if (index == -1) {
      if (!primitiveValue.contains(val)) _primitiveValue.add(val);
    } else {
      _primitiveValue.insert(index, val);
    }

    fire('primitiveValue');

    this.updateD3EChanges('primitiveValue');
  }

  void removeFromPrimitiveValue(String val) {
    _primitiveValue.remove(val);

    fire('primitiveValue');

    this.updateD3EChanges('primitiveValue');
  }

  List<ModelTestData> get modelValue {
    return _modelValue;
  }

  void setModelValue(List<ModelTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_modelValue, val);

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('modelValue');

    updateObservableColl('modelValue', _modelValue, val);

    _modelValue.clear();

    _modelValue.addAll(val);

    fire('modelValue');
  }

  void addToModelValue(ModelTestData val, [int index = -1]) {
    if (index == -1) {
      if (!modelValue.contains(val)) _modelValue.add(val);
    } else {
      _modelValue.insert(index, val);
    }

    fire('modelValue');

    this.updateD3EChanges('modelValue');
  }

  void removeFromModelValue(ModelTestData val) {
    _modelValue.remove(val);

    fire('modelValue');

    this.updateD3EChanges('modelValue');
  }

  List<StructTestData> get structValue {
    return _structValue;
  }

  void setStructValue(List<StructTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_structValue, val);

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('structValue');

    updateObservableColl('structValue', _structValue, val);

    _structValue.clear();

    _structValue.addAll(val);

    fire('structValue');
  }

  void addToStructValue(StructTestData val, [int index = -1]) {
    if (index == -1) {
      if (!structValue.contains(val)) _structValue.add(val);
    } else {
      _structValue.insert(index, val);
    }

    fire('structValue');

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('structValue');
  }

  void removeFromStructValue(StructTestData val) {
    _structValue.remove(val);

    fire('structValue');

    this.updateD3EChanges('structValue');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ModelTestData get masterModelTestData {
    return _masterModelTestData;
  }

  set masterModelTestData(ModelTestData val) {
    _masterModelTestData = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  StructTestData get masterStructTestData {
    return _masterStructTestData;
  }

  set masterStructTestData(StructTestData val) {
    _masterStructTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);

    this._masterModelTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterStructTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  PropertyTestData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    PropertyTestData obj = (dbObj as PropertyTestData);

    obj._id = _id;

    obj.setIdentity(_identity);

    obj.setPrimitiveValue(_primitiveValue);

    obj.setModelValue(_modelValue);

    obj.setStructValue(ctx.cloneRefList(_structValue));
  }
}

class PropertyTestDataReader<T extends PropertyTestData>
    implements JsonReader<T> {
  T createNewInstance(int id) {
    PropertyTestData obj = PropertyTestData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('identity')) {
      obj.setIdentity(ctx.readString('identity'));
    }

    if (ctx.has('primitiveValue')) {
      obj.setPrimitiveValue(ctx.readStringList('primitiveValue'));
    }

    if (ctx.has('modelValue')) {
      ctx.readRefList<ModelTestData>('modelValue', 'ModelTestData',
          obj._modelValue, (val) => obj.setModelValue(val));
    }

    if (ctx.has('structValue')) {
      ctx.readRefList<StructTestData>('structValue', 'StructTestData',
          obj._structValue, (val) => obj.setStructValue(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('identity')) {
      jsonMap['identity'] = obj._identity;
    }

    if (obj.d3eChanges.contains('primitiveValue')) {
      jsonMap['primitiveValue'] = obj._primitiveValue;
    }

    if (obj.d3eChanges.contains('modelValue')) {
      jsonMap['modelValue'] = ctx.writeRefList<ModelTestData>(obj._modelValue);
    }

    if (obj.d3eChanges.contains('structValue')) {
      jsonMap['structValue'] =
          ctx.writeRefList<StructTestData>(obj._structValue);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
