import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'PropertyTestData.dart';

class ModelTestData extends ObjectObservable implements DBObject {
  static String D3EFullQuery =
      '{ id identity data{ id identity primitiveValue modelValue{ id } structValue{ id } } }';
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _identity = '';
  List<PropertyTestData> _data = [];
  List<CloneContext> _save_points = List();
  ModelTestData({List<PropertyTestData> data, String identity}) {
    this.setData(data ?? []);

    this.setIdentity(identity ?? '');
  }
  String get d3eType {
    return 'ModelTestData';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get identity {
    return _identity;
  }

  void setIdentity(String val) {
    bool isValChanged = _identity != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('identity');

    _identity = val;

    fire('identity');
  }

  List<PropertyTestData> get data {
    return _data;
  }

  void setData(List<PropertyTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterModelTestData = null);
    }

    if (val != null) {
      for (PropertyTestData o in val) {
        o.masterModelTestData = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    {
      for (PropertyTestData o in val) {
        o.setInValue(null);
      }
    }

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyTestData val, [int index = -1]) {
    val.masterModelTestData = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    {
      val.setInValue(null);
    }

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyTestData val) {
    _data.remove(val);

    val.masterModelTestData = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);
  }

  ModelTestData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    ModelTestData obj = (dbObj as ModelTestData);

    obj._id = _id;

    obj.setIdentity(_identity);

    ctx.cloneChildList(_data, (v) => obj.setData(v));
  }
}

class ModelTestDataReader<T extends ModelTestData> implements JsonReader<T> {
  T createNewInstance(int id) {
    ModelTestData obj = ModelTestData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('identity')) {
      obj.setIdentity(ctx.readString('identity'));
    }

    if (ctx.has('data')) {
      ctx.readRefList<PropertyTestData>(
          'data', 'PropertyTestData', obj._data, (val) => obj.setData(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('identity')) {
      jsonMap['identity'] = obj._identity;
    }

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyTestData>(obj._data);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
