import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CIfData.dart';
import 'CNodeData.dart';
import 'CPopupData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';
import 'PropertyTestData.dart';

class CForData extends CNodeData implements DBObject {
  List<PropertyTestData> _data = [];
  List<CNodeData> _items = [];
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CForData(
      {List<PropertyTestData> data,
      double height,
      String identity,
      List<CNodeData> items,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setData(data ?? []);

    this.setItems(items ?? []);
  }
  String get d3eType {
    return 'CForData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  List<PropertyTestData> get data {
    return _data;
  }

  void setData(List<PropertyTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterCForData = null);
    }

    if (val != null) {
      for (PropertyTestData o in val) {
        o.masterCForData = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    {
      for (PropertyTestData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyTestData val, [int index = -1]) {
    val.masterCForData = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyTestData val) {
    _data.remove(val);

    val.masterCForData = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  List<CNodeData> get items {
    return _items;
  }

  void setItems(List<CNodeData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_items, val);

    if (!isValChanged) {
      return;
    }

    if (_items != null) {
      _items.forEach((one) => one.masterCForData = null);
    }

    if (val != null) {
      for (CNodeData o in val) {
        o.masterCForData = this;

        o.childPropertyInMaster = 'items';
      }
    }

    this.updateD3EChanges('items');

    updateObservableColl('items', _items, val);

    {
      for (CNodeData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _items.clear();

    _items.addAll(val);

    fire('items');
  }

  void addToItems(CNodeData val, [int index = -1]) {
    val.masterCForData = this;

    val.childPropertyInMaster = 'items';

    if (index == -1) {
      if (!items.contains(val)) _items.add(val);
    } else {
      _items.insert(index, val);
    }

    fire('items');

    updateObservable('items', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('items');
  }

  void removeFromItems(CNodeData val) {
    _items.remove(val);

    val.masterCForData = null;

    fire('items');

    removeObservable('items', val);

    this.updateD3EChanges('items');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CForData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CForData obj = (dbObj as CForData);

    ctx.cloneChildList(_data, (v) => obj.setData(v));

    ctx.cloneChildList(_items, (v) => obj.setItems(v));
  }
}

class CForDataReader<T extends CForData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CForData obj = CForData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('data')) {
      ctx.readRefList<PropertyTestData>(
          'data', 'PropertyTestData', obj._data, (val) => obj.setData(val));
    }

    if (ctx.has('items')) {
      ctx.readRefList<CNodeData>(
          'items', 'CNodeData', obj._items, (val) => obj.setItems(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyTestData>(obj._data);
    }

    if (obj.d3eChanges.contains('items')) {
      jsonMap['items'] = ctx.writeObjUnionList<CNodeData>(obj._items);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
