import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CIfData.dart';
import 'CNodeData.dart';
import 'CPopupData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';

class CSlotData extends CNodeData implements DBObject {
  String _slot = '';
  CRefSlotData _actual;
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CSlotData(
      {CRefSlotData actual,
      double height,
      String identity,
      String slot,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setActual(actual ?? null);

    this.setSlot(slot ?? '');
  }
  String get d3eType {
    return 'CSlotData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  String get slot {
    return _slot;
  }

  void setSlot(String val) {
    bool isValChanged = _slot != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('slot');

    _slot = val;

    fire('slot');
  }

  CRefSlotData get actual {
    return _actual;
  }

  void setActual(CRefSlotData val) {
    bool isValChanged = _actual != val;

    if (!isValChanged) {
      return;
    }

    if (_actual != null) {
      _actual.masterCSlotData = null;
    }

    if (val != null) {
      val.masterCSlotData = this;

      val.childPropertyInMaster = 'actual';
    }

    this.updateD3EChanges('actual');

    updateObservable('actual', _actual, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _actual = val;

    fire('actual');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CSlotData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CSlotData obj = (dbObj as CSlotData);

    obj.setSlot(_slot);

    ctx.cloneChild(_actual, (v) => obj.setActual(v));
  }
}

class CSlotDataReader<T extends CSlotData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CSlotData obj = CSlotData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('slot')) {
      obj.setSlot(ctx.readString('slot'));
    }

    if (ctx.has('actual')) {
      ctx.readRef<CRefSlotData>(
          'actual', 'CRefSlotData', obj._actual, (val) => obj.setActual(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('slot')) {
      jsonMap['slot'] = obj._slot;
    }

    if (obj.d3eChanges.contains('actual')) {
      jsonMap['actual'] = ctx.writeObj<CRefSlotData>(obj._actual);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
