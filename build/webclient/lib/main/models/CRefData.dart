import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/CollectionUtils.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CIfData.dart';
import 'CNodeData.dart';
import 'CPopupData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';
import 'PropertyTestData.dart';

class CRefData extends CNodeData implements DBObject {
  List<PropertyTestData> _data = [];
  CNodeData _builder;
  CNodeData _buildData;
  CNodeData _child;
  List<CNodeData> _children = [];
  List<CRefSlotData> _slots = [];
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CRefData(
      {CNodeData buildData,
      CNodeData builder,
      CNodeData child,
      List<CNodeData> children,
      List<PropertyTestData> data,
      double height,
      String identity,
      List<CRefSlotData> slots,
      double width,
      double x,
      double y})
      : super(height: height, identity: identity, width: width, x: x, y: y) {
    this.setBuildData(buildData ?? null);

    this.setBuilder(builder ?? null);

    this.setChild(child ?? null);

    this.setChildren(children ?? []);

    this.setData(data ?? []);

    this.setSlots(slots ?? []);
  }
  String get d3eType {
    return 'CRefData';
  }

  void subscribe(SubscriptionContext ctx) {
    super.subscribe(ctx);
  }

  List<PropertyTestData> get data {
    return _data;
  }

  void setData(List<PropertyTestData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_data, val);

    if (!isValChanged) {
      return;
    }

    if (_data != null) {
      _data.forEach((one) => one.masterCRefData = null);
    }

    if (val != null) {
      for (PropertyTestData o in val) {
        o.masterCRefData = this;

        o.childPropertyInMaster = 'data';
      }
    }

    this.updateD3EChanges('data');

    updateObservableColl('data', _data, val);

    {
      for (PropertyTestData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _data.clear();

    _data.addAll(val);

    fire('data');
  }

  void addToData(PropertyTestData val, [int index = -1]) {
    val.masterCRefData = this;

    val.childPropertyInMaster = 'data';

    if (index == -1) {
      if (!data.contains(val)) _data.add(val);
    } else {
      _data.insert(index, val);
    }

    fire('data');

    updateObservable('data', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('data');
  }

  void removeFromData(PropertyTestData val) {
    _data.remove(val);

    val.masterCRefData = null;

    fire('data');

    removeObservable('data', val);

    this.updateD3EChanges('data');
  }

  CNodeData get builder {
    return _builder;
  }

  void setBuilder(CNodeData val) {
    bool isValChanged = _builder != val;

    if (!isValChanged) {
      return;
    }

    if (_builder != null) {
      _builder.masterCRefData = null;
    }

    if (val != null) {
      val.masterCRefData = this;

      val.childPropertyInMaster = 'builder';
    }

    this.updateD3EChanges('builder');

    updateObservable('builder', _builder, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _builder = val;

    fire('builder');
  }

  CNodeData get buildData {
    return _buildData;
  }

  void setBuildData(CNodeData val) {
    bool isValChanged = _buildData != val;

    if (!isValChanged) {
      return;
    }

    if (_buildData != null) {
      _buildData.masterCRefData = null;
    }

    if (val != null) {
      val.masterCRefData = this;

      val.childPropertyInMaster = 'buildData';
    }

    this.updateD3EChanges('buildData');

    updateObservable('buildData', _buildData, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _buildData = val;

    fire('buildData');
  }

  CNodeData get child {
    return _child;
  }

  void setChild(CNodeData val) {
    bool isValChanged = _child != val;

    if (!isValChanged) {
      return;
    }

    if (_child != null) {
      _child.masterCRefData = null;
    }

    if (val != null) {
      val.masterCRefData = this;

      val.childPropertyInMaster = 'child';
    }

    this.updateD3EChanges('child');

    updateObservable('child', _child, val);

    {
      if (val != null) {
        val.setInValue(this.inValue);
      }
    }

    _child = val;

    fire('child');
  }

  List<CNodeData> get children {
    return _children;
  }

  void setChildren(List<CNodeData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_children, val);

    if (!isValChanged) {
      return;
    }

    if (_children != null) {
      _children.forEach((one) => one.masterCRefData = null);
    }

    if (val != null) {
      for (CNodeData o in val) {
        o.masterCRefData = this;

        o.childPropertyInMaster = 'children';
      }
    }

    this.updateD3EChanges('children');

    updateObservableColl('children', _children, val);

    {
      for (CNodeData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _children.clear();

    _children.addAll(val);

    fire('children');
  }

  void addToChildren(CNodeData val, [int index = -1]) {
    val.masterCRefData = this;

    val.childPropertyInMaster = 'children';

    if (index == -1) {
      if (!children.contains(val)) _children.add(val);
    } else {
      _children.insert(index, val);
    }

    fire('children');

    updateObservable('children', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('children');
  }

  void removeFromChildren(CNodeData val) {
    _children.remove(val);

    val.masterCRefData = null;

    fire('children');

    removeObservable('children', val);

    this.updateD3EChanges('children');
  }

  List<CRefSlotData> get slots {
    return _slots;
  }

  void setSlots(List<CRefSlotData> val) {
    bool isValChanged = CollectionUtils.isNotEquals(_slots, val);

    if (!isValChanged) {
      return;
    }

    if (_slots != null) {
      _slots.forEach((one) => one.masterCRefData = null);
    }

    if (val != null) {
      for (CRefSlotData o in val) {
        o.masterCRefData = this;

        o.childPropertyInMaster = 'slots';
      }
    }

    this.updateD3EChanges('slots');

    updateObservableColl('slots', _slots, val);

    {
      for (CRefSlotData o in val) {
        o.setInValue(this.inValue);
      }
    }

    _slots.clear();

    _slots.addAll(val);

    fire('slots');
  }

  void addToSlots(CRefSlotData val, [int index = -1]) {
    val.masterCRefData = this;

    val.childPropertyInMaster = 'slots';

    if (index == -1) {
      if (!slots.contains(val)) _slots.add(val);
    } else {
      _slots.insert(index, val);
    }

    fire('slots');

    updateObservable('slots', null, val);

    {
      val.setInValue(this.inValue);
    }

    this.updateD3EChanges('slots');
  }

  void removeFromSlots(CRefSlotData val) {
    _slots.remove(val);

    val.masterCRefData = null;

    fire('slots');

    removeObservable('slots', val);

    this.updateD3EChanges('slots');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    super.updateD3EChanges(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CRefData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    super.deepCloneIntoObj(dbObj, ctx);

    CRefData obj = (dbObj as CRefData);

    ctx.cloneChildList(_data, (v) => obj.setData(v));

    ctx.cloneChild(_builder, (v) => obj.setBuilder(v));

    ctx.cloneChild(_buildData, (v) => obj.setBuildData(v));

    ctx.cloneChild(_child, (v) => obj.setChild(v));

    ctx.cloneChildList(_children, (v) => obj.setChildren(v));

    ctx.cloneChildList(_slots, (v) => obj.setSlots(v));
  }
}

class CRefDataReader<T extends CRefData> extends CNodeDataReader<T> {
  T createNewInstance(int id) {
    CRefData obj = CRefData();

    obj.id = id;

    return obj;
  }

  void fromJson(ReaderContext ctx, T obj) {
    super.fromJson(ctx, obj);

    obj.saveStatus = DBSaveStatus.Saved;

    if (ctx.has('data')) {
      ctx.readRefList<PropertyTestData>(
          'data', 'PropertyTestData', obj._data, (val) => obj.setData(val));
    }

    if (ctx.has('builder')) {
      ctx.readRef<CNodeData>(
          'builder', 'CNodeData', obj._builder, (val) => obj.setBuilder(val));
    }

    if (ctx.has('buildData')) {
      ctx.readRef<CNodeData>('buildData', 'CNodeData', obj._buildData,
          (val) => obj.setBuildData(val));
    }

    if (ctx.has('child')) {
      ctx.readRef<CNodeData>(
          'child', 'CNodeData', obj._child, (val) => obj.setChild(val));
    }

    if (ctx.has('children')) {
      ctx.readRefList<CNodeData>('children', 'CNodeData', obj._children,
          (val) => obj.setChildren(val));
    }

    if (ctx.has('slots')) {
      ctx.readRefList<CRefSlotData>(
          'slots', 'CRefSlotData', obj._slots, (val) => obj.setSlots(val));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = super.toJson(ctx, obj);

    if (obj.d3eChanges.contains('data')) {
      jsonMap['data'] = ctx.writeObjList<PropertyTestData>(obj._data);
    }

    if (obj.d3eChanges.contains('builder')) {
      jsonMap['builder'] = ctx.writeObjUnion<CNodeData>(obj._builder);
    }

    if (obj.d3eChanges.contains('buildData')) {
      jsonMap['buildData'] = ctx.writeObjUnion<CNodeData>(obj._buildData);
    }

    if (obj.d3eChanges.contains('child')) {
      jsonMap['child'] = ctx.writeObjUnion<CNodeData>(obj._child);
    }

    if (obj.d3eChanges.contains('children')) {
      jsonMap['children'] = ctx.writeObjUnionList<CNodeData>(obj._children);
    }

    if (obj.d3eChanges.contains('slots')) {
      jsonMap['slots'] = ctx.writeObjList<CRefSlotData>(obj._slots);
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
