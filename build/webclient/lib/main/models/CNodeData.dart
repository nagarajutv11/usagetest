import '../utils/ObjectObservable.dart';
import '../utils/JSONUtils.dart';
import '../utils/CloneContext.dart';
import '../utils/DBObject.dart';
import '../utils/DBSaveStatus.dart';
import '../utils/JsonReader.dart';
import '../utils/ReaderContext.dart';
import '../utils/SubscriptionContext.dart';
import 'CForData.dart';
import 'CIfData.dart';
import 'CPopupData.dart';
import 'CRefData.dart';
import 'CRefSlotData.dart';
import 'CSwitchData.dart';
import 'ComponentTestData.dart';

abstract class CNodeData extends ObjectObservable implements DBObject {
  int _id = 0;
  DBSaveStatus _saveStatus = DBSaveStatus.New;
  Set<String> d3eChanges = {};
  String _identity = '';
  double _x = 0.0;
  double _y = 0.0;
  double _width = 0.0;
  double _height = 0.0;
  ComponentTestData _in;
  String _childPropertyInMaster = '';
  ComponentTestData _masterComponentTestData;
  CForData _masterCForData;
  CIfData _masterCIfData;
  CPopupData _masterCPopupData;
  CRefData _masterCRefData;
  CRefSlotData _masterCRefSlotData;
  CSwitchData _masterCSwitchData;
  List<CloneContext> _save_points = List();
  CNodeData(
      {double height, String identity, double width, double x, double y}) {
    this.setHeight(height ?? 0.0);

    this.setIdentity(identity ?? '');

    this.setWidth(width ?? 0.0);

    this.setX(x ?? 0.0);

    this.setY(y ?? 0.0);
  }
  String get d3eType {
    return 'CNodeData';
  }

  void subscribe(SubscriptionContext ctx) {}
  int get id {
    return _id;
  }

  set id(int val) {
    _id = val;
  }

  DBSaveStatus get saveStatus {
    return _saveStatus;
  }

  set saveStatus(DBSaveStatus val) {
    _saveStatus = val;
  }

  String get identity {
    return _identity;
  }

  void setIdentity(String val) {
    bool isValChanged = _identity != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('identity');

    _identity = val;

    fire('identity');
  }

  double get x {
    return _x;
  }

  void setX(double val) {
    bool isValChanged = _x != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('x');

    _x = val;

    fire('x');
  }

  double get y {
    return _y;
  }

  void setY(double val) {
    bool isValChanged = _y != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('y');

    _y = val;

    fire('y');
  }

  double get width {
    return _width;
  }

  void setWidth(double val) {
    bool isValChanged = _width != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('width');

    _width = val;

    fire('width');
  }

  double get height {
    return _height;
  }

  void setHeight(double val) {
    bool isValChanged = _height != val;

    if (!isValChanged) {
      return;
    }

    this.updateD3EChanges('height');

    _height = val;

    fire('height');
  }

  ComponentTestData get inValue {
    return _in;
  }

  void setInValue(ComponentTestData val) {
    bool isValChanged = _in != val;

    if (!isValChanged) {
      return;
    }

    _in = val;

    fire('in');
  }

  String get childPropertyInMaster {
    return _childPropertyInMaster;
  }

  set childPropertyInMaster(String val) {
    _childPropertyInMaster = val;
  }

  ComponentTestData get masterComponentTestData {
    return _masterComponentTestData;
  }

  set masterComponentTestData(ComponentTestData val) {
    _masterComponentTestData = val;
  }

  CForData get masterCForData {
    return _masterCForData;
  }

  set masterCForData(CForData val) {
    _masterCForData = val;
  }

  CIfData get masterCIfData {
    return _masterCIfData;
  }

  set masterCIfData(CIfData val) {
    _masterCIfData = val;
  }

  CPopupData get masterCPopupData {
    return _masterCPopupData;
  }

  set masterCPopupData(CPopupData val) {
    _masterCPopupData = val;
  }

  CRefData get masterCRefData {
    return _masterCRefData;
  }

  set masterCRefData(CRefData val) {
    _masterCRefData = val;
  }

  CRefSlotData get masterCRefSlotData {
    return _masterCRefSlotData;
  }

  set masterCRefSlotData(CRefSlotData val) {
    _masterCRefSlotData = val;
  }

  CSwitchData get masterCSwitchData {
    return _masterCSwitchData;
  }

  set masterCSwitchData(CSwitchData val) {
    _masterCSwitchData = val;
  }

  void updateD3EChanges(String change) {
    this.d3eChanges.add(change);

    this._masterComponentTestData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCForData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCIfData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCPopupData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCRefSlotData?.updateD3EChanges(this.childPropertyInMaster);

    this._masterCSwitchData?.updateD3EChanges(this.childPropertyInMaster);
  }

  CNodeData deepClone({newObj = false}) {
    CloneContext ctx = CloneContext();

    return ctx.startClone(this, newObj: newObj);
  }

  void save() {
    CloneContext ctx = CloneContext();

    ctx.startClone(this);

    _save_points.add(ctx);
  }

  void restore() {
    if (_save_points.isNotEmpty) {
      CloneContext ctx = _save_points.removeLast();

      ctx.revert(this);
    }
  }

  void deepCloneIntoObj(DBObject dbObj, CloneContext ctx) {
    CNodeData obj = (dbObj as CNodeData);

    obj._id = _id;

    obj.setIdentity(_identity);

    obj.setX(_x);

    obj.setY(_y);

    obj.setWidth(_width);

    obj.setHeight(_height);
  }
}

abstract class CNodeDataReader<T extends CNodeData> implements JsonReader<T> {
  T createNewInstance(int id);
  void fromJson(ReaderContext ctx, T obj) {
    if (ctx.has('identity')) {
      obj.setIdentity(ctx.readString('identity'));
    }

    if (ctx.has('x')) {
      obj.setX(ctx.readDouble('x'));
    }

    if (ctx.has('y')) {
      obj.setY(ctx.readDouble('y'));
    }

    if (ctx.has('width')) {
      obj.setWidth(ctx.readDouble('width'));
    }

    if (ctx.has('height')) {
      obj.setHeight(ctx.readDouble('height'));
    }
  }

  Map toJson(ReaderContext ctx, T obj) {
    if (obj == null) {
      return null;
    }

    Map jsonMap = Map();

    jsonMap['id'] = obj._id;

    if (obj.d3eChanges.contains('identity')) {
      jsonMap['identity'] = obj._identity;
    }

    if (obj.d3eChanges.contains('x')) {
      jsonMap['x'] = obj._x;
    }

    if (obj.d3eChanges.contains('y')) {
      jsonMap['y'] = obj._y;
    }

    if (obj.d3eChanges.contains('width')) {
      jsonMap['width'] = obj._width;
    }

    if (obj.d3eChanges.contains('height')) {
      jsonMap['height'] = obj._height;
    }

    return jsonMap;
  }

  void clear(T obj) {
    obj.d3eChanges.clear();
  }
}
