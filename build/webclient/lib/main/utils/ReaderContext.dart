import 'DBObject.dart';
import 'BaseObject.dart';
import 'ReferenceCatch.dart';
import 'SubscriptionQuery.dart';
import 'SubscriptionHolder.dart';
import '../classes/Subscription.dart';
import 'JsonReader.dart';
import 'ReaderProvider.dart';
import 'package:d3e_lang_dart/core.dart';

class ReaderContext {
  BaseObject parent;
  String refField;
  Map<String, dynamic> json;
  ReferenceCatch refCatch;
  static int _localObjCount = -2;

  ReaderContext(this.json, this.refCatch, [this.parent, this.refField]);

  bool has(String field) {
    return json.containsKey(field);
  }

  String readString(String field) {
    return json[field];
  }

  double readDouble(String field) {
    return json[field];
  }

  int readInteger(String field) {
    return json[field];
  }

  bool readBoolean(String field) {
    return json[field];
  }

  ExpressionString readExpressionString(String field) {
    String content = json[field] as String;
    if (content == null) return null;
    return ExpressionString(content);
  }

  BlockString readBlockString(String field) {
    String content = json[field] as String;
    if (content == null) return null;
    return BlockString(content);
  }

  DateTime readDateTime(String field) {
    String dateTime = json[field] as String;
    return DateTime.parse(dateTime);
  }

  DFile readDFile(String field) {
    return DFile.fromJson(json[field]);
  }

  Duration readDuration(String field) {
    return json[field] as Duration;
  }

  Time readTime(String field) {
    return json[field] as Time;
  }

  List<Time> readTimeList(String field) {
    // TODO
    throw UnsupportedError('This operation is not supported');
  }

  Set<Time> readTimeSet(String field) {
    // TODO
    throw UnsupportedError('This operation is not supported');
  }

  List<DFile> readDFileList(String field) {
    // TODO
    return [];
  }

  Set<DFile> readDFileSet(String field) {
    // TODO
    throw UnsupportedError('This operation is not supported');
  }

  BaseObject _readObj(String type, Map<String, dynamic> objJson) {
    if (objJson == null) {
      return null;
    }
    int id = objJson['id'];
    type = objJson['__typename'] ?? type;
    JsonReader reader = ReaderProvider.get().getReader(type);
    BaseObject obj = refCatch.findObject(type, id);
    if (obj == null) {
      obj = reader.createNewInstance(id);
      if (obj is DBObject) {
        refCatch.addObject(obj);
      }
    }
    return obj;
  }

  T _refReadHelper<T extends BaseObject>(T obj, T old) {
    if (obj is DBObject) {
      refCatch.updateReference(old as DBObject, obj);
    }
    if (old == obj) {
      return obj;
    }
    return obj;
  }

  T readRefStart<T extends BaseObject>(String field, String type) {
    BaseObject obj = _readObj(type, json[field]);
    _refReadHelper(obj, null);
    readRefFull(field, type, obj);
    return obj;
  }

  void readRef<T extends BaseObject>(
      String field, String type, T old, Consumer<T> setter) {
    BaseObject obj = _readObj(type, json[field]);
    _refReadHelper(obj, old);
    setter(obj);
    readRefFull(field, type, obj);
    resubscribe(field, old, obj);
  }

  void readRefFull<T extends BaseObject>(String field, String type, T obj) {
    if (obj == null) {
      return;
    }
    JsonReader reader = ReaderProvider.get().getReader(obj.d3eType);
    reader.fromJson(ReaderContext(json[field], refCatch, obj, field), obj);
    reader.clear(obj);
  }

  void resubscribe(String field, BaseObject old, BaseObject obj) {
    if (parent is DBObject) {
      Subscription sub = Subscription.get();
      SubscriptionHolder parentHolder = sub.getHolder(parent.d3eType);
      if (parentHolder != null) {
        SubscriptionQuery query =
            parentHolder.getRefContext(field, (parent as DBObject).id);
        if (query != null) {
          query.resubscribe(old, obj);
        }
      }
    }
  }

  void resubscribeList(
      String field, List<BaseObject> olds, List<BaseObject> objs) {
    if (parent is DBObject) {
      Subscription sub = Subscription.get();
      SubscriptionHolder parentHolder = sub.getHolder(parent.d3eType);
      if (parentHolder != null) {
        SubscriptionQuery query =
            parentHolder.getRefContext(field, (parent as DBObject).id);
        if (query != null) {
          query.resubscribeCollection(olds, objs);
        }
      }
    }
  }

  T readAny<T extends BaseObject>() {
    BaseObject obj = _readObj(json['__typename'], json);
    readRefFull(null, json['__typename'], obj);
    return obj;
  }

  T readEnum<T>(String field, T convert(String enumStr)) {
    return convert(json[field]);
  }

  void readRefSet<T extends BaseObject>(
      String field, String type, Set<T> old, Consumer<Set<T>> setter) {
    readRefList(field, type, old.toList(), (val) => setter(val.toSet()));
  }

  void readRefList<T extends BaseObject>(
      String field, String type, List<T> old, Consumer<List<T>> setter) {
    List<T> ans = [];
    List<dynamic> current = json[field] as List<dynamic>;
    for (int i = 0; i < current.length; i++) {
      BaseObject obj = _readObj(type, current.elementAt(i));
      T oldObj = i < old.length ? old.elementAt(i) : null;
      ans.add(_refReadHelper(obj, oldObj));
    }
    setter(ans);
    for (int i = 0; i < ans.length; i++) {
      T obj = ans[i];
      if (obj == null) {
        continue;
      }
      JsonReader reader = ReaderProvider.get().getReader(obj.d3eType);
      reader.fromJson(
          ReaderContext(current.elementAt(i), refCatch, obj, field), obj);
      reader.clear(obj);
    }
    resubscribeList(field, old, ans);
  }

  void readListFull<T extends BaseObject>(
      String field, String type, List<T> ans) {
    List<dynamic> current = json[field] as List<dynamic>;
    for (int i = 0; i < ans.length; i++) {
      T obj = ans[i];
      if (obj == null) {
        continue;
      }
      JsonReader reader = ReaderProvider.get().getReader(obj.d3eType);
      reader.fromJson(
          ReaderContext(current.elementAt(i), refCatch, obj, field), obj);
      reader.clear(obj);
    }
  }

  List<String> readStringList(String field) {
    if (json[field] == null) {
      return [];
    }
    return (json[field] as List<dynamic>).map((s) => s as String).toList();
  }

  List<int> readIntegerList(String field) {
    return (json[field] as List<dynamic>).map((s) => s as int).toList();
  }

  List<bool> readBooleanList(String field) {
    return (json[field] as List<dynamic>).map((s) => s as bool).toList();
  }

  List<T> readEnumList<T>(String field, T convert(String enumStr)) {
    List<dynamic> current = json[field] as List<dynamic>;
    return current.map((x) => convert(x as String)).toList();
  }

  /* Writer methods */
  Map writeObj<T extends DBObject>(T obj) {
    if (obj == null) {
      return null;
    }
    String type = obj.d3eType;
    if (obj.id == 0) {
      obj.id = nextLocalId();
    }
    JsonReader reader = ReaderProvider.get().getReader(type);
    Map ans = reader.toJson(this, obj);
    reader.clear(obj);
    return ans;
  }

  Map writeObjUnion<T extends DBObject>(T obj) {
    if (obj == null) {
      return null;
    }
    String type = obj.d3eType;
    String indexType = 'value' + type;
    Map ans = Map();
    ans['type'] = type;
    ans[indexType] = writeObj(obj);
    return ans;
  }

  List<Map> writeObjUnionList<T extends DBObject>(List<T> list) {
    return list?.map((one) => writeObjUnion(one))?.toList();
  }

  List<Map> writeObjUnionSet<T extends DBObject>(Set<T> list) {
    return list?.map((one) => writeObjUnion(one))?.toList();
  }

  List<Map> writeRefList<T extends DBObject>(List<T> list) {
    return list?.map((one) => writeRef(one))?.toList();
  }

  List<Map> writeRefSet<T extends DBObject>(Set<T> list) {
    return list?.map((one) => writeRef(one))?.toList();
  }

  List<Map> writeObjList<T extends DBObject>(List<T> list) {
    return list?.map((one) => writeObj(one))?.toList();
  }

  List<Map> writeObjSet<T extends DBObject>(Set<T> list) {
    return list?.map((one) => writeObj(one))?.toList();
  }

  Map writeRef<T extends DBObject>(T obj) {
    if (obj == null) {
      return null;
    } else if (obj.id == 0) {
      obj.id = nextLocalId();
    }
    Map ans = Map();
    ans['type'] = obj.d3eType;
    ans['id'] = obj.id;
    return ans;
  }

  int nextLocalId() {
    _localObjCount = _localObjCount - 1;
    return _localObjCount;
  }
}
