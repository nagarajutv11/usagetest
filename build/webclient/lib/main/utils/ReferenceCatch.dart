import 'DBObject.dart';

class ReferenceCatch {
  Map<String, DBObject> all = {};
  Map<String, int> refCounts = {};

  ReferenceCatch();

  static ReferenceCatch _instance;

  factory ReferenceCatch.get() {
    if (_instance == null) {
      _instance = ReferenceCatch();
    }
    return _instance;
  }

  DBObject findObject(String type, int id) {
    if (id == null || id == 0) {
      return null;
    }
    return all[prepareKey2(type, id)];
  }

  void addObject(DBObject obj) {
    if (obj.id == null || obj.id == 0) {
      return;
    }
    all[prepareKey(obj)] = obj;
  }

  void updateReference(DBObject old, DBObject obj) {
    if (old == obj) {
      return;
    }
    if (old != null) {
      int count = refCounts[prepareKey(old)];
      if (count != null) {
        refCounts[prepareKey(old)] = count - 1;
      }
    }
    if (obj != null) {
      int count = refCounts[prepareKey(obj)];
      if (count == null) {
        count = 0;
      }
      refCounts[prepareKey(obj)] = count + 1;
    }
  }

  String prepareKey(DBObject obj) {
    return prepareKey2(obj.d3eType, obj.id);
  }

  String prepareKey2(String type, int id) {
    return type + '-' + id.toString();
  }
}
