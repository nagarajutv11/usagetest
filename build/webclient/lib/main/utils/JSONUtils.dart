import 'package:d3e_lang_dart/core.dart';

class JSONUtils {
  static ExpressionString stringToExpressionString(String eString) {
    if (eString == null) {
      return null;
    }

    return ExpressionString(eString);
  }

  static String expressionStringToString(ExpressionString eString) {
    return eString?.content;
  }

  static BlockString stringToBlockString(String bString) {
    if (bString == null) {
      return null;
    }

    return BlockString(bString);
  }

  static String blockStringToString(BlockString bString) {
    return bString?.content;
  }
}
