import 'package:d3e_studio/main/classes/Subscription.dart';

import 'SubscriptionContext.dart';
import 'BaseObject.dart';

class SubscriptionQuery {
  Map<String, SubscriptionQuery> fields;
  Map<String, Map<String, SubscriptionQuery>> subTypes;
  bool creatable;
  bool flat;

  SubscriptionQuery(
      {this.creatable = false, this.fields, this.subTypes, this.flat = false});

  void resubscribe(BaseObject old, BaseObject obj) {
    if (old == obj) {
      return;
    }
    if (obj != null) {
      SubscriptionContext ctx = SubscriptionContext(this, true, {});
      ctx.subscribeCollectionByType(obj.d3eType, [obj]);
    }

    if (old != null) {
      SubscriptionContext ctx = SubscriptionContext(this, false, {});
      ctx.unsubscribeCollectionByType(old.d3eType, [old]);
    }
  }

  void resubscribeCollection(Iterable oldColl, Iterable objColl) {
    SubscriptionContext ctx = SubscriptionContext(this, true, {});
    Map<String, List<BaseObject>> objByType = ctx.groupByType(objColl);
    objByType.forEach((key, value) {
      ctx.subscribeCollectionByType(key, value);
    });

    ctx = SubscriptionContext(this, false, {});
    Map<String, List<BaseObject>> oldByType = ctx.groupByType(oldColl);
    oldByType.forEach((key, value) {
      ctx.unsubscribeCollectionByType(key, value);
    });
  }
}
