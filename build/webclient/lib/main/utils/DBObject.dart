import 'CloneContext.dart';
import 'BaseObject.dart';
import 'DBSaveStatus.dart';

abstract class DBObject extends BaseObject {
  int get id;
  set id(int id);
  DBSaveStatus get saveStatus;
  set saveStatus(DBSaveStatus saveStatus);
  void deepCloneIntoObj(DBObject obj, CloneContext ctx);
}
