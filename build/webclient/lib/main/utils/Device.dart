import 'DBObject.dart';
import 'ReaderProvider.dart';
import 'ReferenceCatch.dart';
import 'package:localstorage/localstorage.dart';

import 'ReaderContext.dart';

class Device {
  static final LocalStorage storage = LocalStorage('auth');
  static final ReferenceCatch _referenceCatch = ReferenceCatch.get();

  static void put(String key, Object value) async {
    if (value is DBObject) {
      ReaderContext ctx = ReaderContext(Map(), _referenceCatch);
      Map json = ctx.writeObj(value);
      // Map json = ReaderProvider.get().getReader(value.d3eType).toJson(value);
      json['__typename'] = value.d3eType;
      await storage.setItem(key, json);
    } else {
      // value is primitive
      await storage.setItem(key, value);
    }
  }

  static Future<Object> get(String key) async {
    // Will be either primitive or Map, since that's what we stored.
    await storage.ready;
    dynamic value = await storage.getItem(key);

    if (value is Map) {
      ReaderContext ctx = ReaderContext(value, _referenceCatch);
      return ctx.readAny();
    }

    return value;
  }
}
