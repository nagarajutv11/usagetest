import 'package:d3e_lang_dart/core.dart';

typedef void PathHandler();

abstract class IObservable {
  void removeObservable(String childPropName, ObjectObservable child);
  void updateObservable(String childPropName, ObjectObservable oldChild,
      ObjectObservable newChild);
  void updateObservableColl(String childPropName,
      List<ObjectObservable> oldChild, List<ObjectObservable> newChild);
  void fire(String path);
  void _fireInternal(String path, Map<IObservable, Set<String>> visited);
  void dispose();
  Set<String> getDependency(String childPropName);
  void addDependency(
      String childPropName, IObservable parent, Set<String> paths);
}

class _Parent {
  IObservable parent;
  Set<String> deps;
  _Parent(this.parent, this.deps);
}

class ObjectObservable implements IObservable {
  bool initDone = false;
  String get nameOfModel {
    return '';
  }

  //Can have more parents with same property name.
  Map<String, List<_Parent>> parents = Map();
  Map<String, List<ObjectObservable>> childs = Map();
  Map<String, List<PathHandler>> listeners = Map();

  void dispose() {
    listeners.clear();
    childs.forEach((k, cs) => cs.forEach((v) => v._removeParent(this, k)));
  }

  void removeObservable(String childPropName, ObjectObservable child) {
    if (child != null) {
      childs.remove(childPropName);
      child._removeParent(this, childPropName);
    }
  }

  void updateObservable(String childPropName, ObjectObservable oldChild,
      ObjectObservable newChild) {
    removeObservable(childPropName, oldChild);
    if (newChild != null) {
      List<ObjectObservable> cs = childs[childPropName];
      if (cs == null) {
        cs = [];
        childs[childPropName] = cs;
      }
      cs.add(newChild);
      newChild._addParent(this, childPropName);
    }
  }

  void updateObservableColl(
      String childPropName,
      Iterable<ObjectObservable> oldChild,
      Iterable<ObjectObservable> newChild) {
    if (oldChild != null) {
      oldChild.forEach((o) => removeObservable(childPropName, o));
    }
    if (newChild != null) {
      newChild.forEach((o) => updateObservable(childPropName, null, o));
    }
  }

  void initListeners() {
    initDone = true;
  }

  void _removeParent(IObservable parent, String childPropName) {
    List<_Parent> parents = this.parents[childPropName];
    if (parents == null) {
      return;
    }
    parents.removeWhere((p) => p.parent == parent);
    if (this.parents.isEmpty) {
      this.dispose();
    }
  }

  void _addParent(IObservable parent, String childPropName) {
    List<_Parent> parents = this.parents[childPropName];
    if (parents == null) {
      parents = [];
      this.parents[childPropName] = parents;
    }
    if (!initDone) {
      this.initListeners();
    }
    Set<String> paths = parent.getDependency(childPropName);
    parents.add(_Parent(parent, paths));
    _addPathsToChildren(paths);
  }

  void _addPathsToChildren(Set<String> paths) {
    this.childs.forEach((key, children) {
      children.forEach((child) => child.addDependency(
          key,
          this,
          paths
              .where((x) => x.startsWith(key + '.'))
              .map((f) => f.substring(key.length + 1))
              .toSet()));
    });
  }

  Set<String> getDependency(String name) {
    return {
      ...listeners.keys,
      ...this.parents.values.expand((f) => f).expand((x) => x.deps)
    }
        .where((x) => x.startsWith(name + '.'))
        .map((f) => f.substring(name.length + 1))
        .toSet();
  }

  void addDependency(
      String childPropName, IObservable parent, Set<String> paths) {
    if (paths.isEmpty) {
      return;
    }
    List<_Parent> parents = this.parents[childPropName];
    _Parent p =
        parents.firstWhere((p) => p.parent == parent, orElse: () => null);
    if (p == null) {
      return;
    }
    p.deps.addAll(paths);
    _addPathsToChildren(paths);
  }

  Runnable on(List<String> paths, PathHandler handler) {
    List<Runnable> disposibles = [];
    paths.forEach((p) {
      List<PathHandler> handlers = listeners[p];
      if (handlers == null) {
        handlers = [];
        listeners[p] = handlers;
      }
      handlers.add(handler);
      disposibles.add(() => handlers.remove(handler));
    });
    _addPathsToChildren(paths.toSet());
    return () => disposibles.forEach((d) => d());
  }

  void fire(String path) {
    Map<IObservable, Set<String>> visited = {};
    _fireInternal(path, visited);
  }

  void _fireInternal(String path, Map<IObservable, Set<String>> visited) {
    if (visited[this] != null && visited[this].contains(path)) {
      return;
    } else {
      if (visited[this] == null) {
        visited[this] = {};
      }
      visited[this].add(path);
    }
    List<PathHandler> handlers = listeners[path];
    if (handlers != null) {
      handlers.forEach((h) => h());
    }
    if (this.nameOfModel != '') {
      childs.forEach((k, cs) => cs.forEach((v) =>
          v._fireInternal('master' + this.nameOfModel + '.' + path, visited)));
    }

    //Inform to master if change is not from master
    if (!path.startsWith('master')) {
      Map<String, List<IObservable>> map;
      this.parents.forEach((name, ps) => ps.forEach((p) {
            if (p.deps.contains(path)) {
              if (map == null) {
                map = Map<String, List<IObservable>>();
              }
              List<IObservable> list = map[name + '.' + path];
              if (list == null) {
                list = [];
                map[name + '.' + path] = list;
              }
              list.add(p.parent);
            }
          }));
      map?.forEach((key, value) {
        value.forEach((i) => i._fireInternal(key, visited));
      });
    }
  }
}
