import 'BaseObject.dart';
import 'ReaderContext.dart';

abstract class JsonReader<T extends BaseObject> {
  T createNewInstance(int id);
  void fromJson(ReaderContext ctx, T obj);
  Map toJson(ReaderContext ctx, T obj);
  void clear(T obj);
}
