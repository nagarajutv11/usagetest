import 'dart:async';

import 'SubscriptionQuery.dart';

class SubscriptionHolder {
  StreamSubscription subscription;
  String type;
  String query;
  String ids;
  //Query and it's Ids
  Map<SubscriptionQuery, List<int>> queries = {};
  //For every reference field, there is Query in each Object;
  Map<String, Map<int, SubscriptionQuery>> refs = {};
  //For every inverse field, there is Query in each Object;
  Map<String, SubscriptionHolder> inverseHolders = {};

  SubscriptionHolder(this.type);

  List<int> getAllIds() {
    return queries.values.expand((e) => e).toSet().toList();
  }

  void addRefContext(String refField, int id, SubscriptionQuery query) {
    Map<int, SubscriptionQuery> byObj = refs[refField];
    if (byObj == null) {
      byObj = {};
      refs[refField] = byObj;
    }
    byObj[id] = query;
  }

  SubscriptionQuery getRefContext(String refField, int id) {
    Map<int, SubscriptionQuery> byObj = refs[refField];
    if (byObj == null) {
      return null;
    }
    return byObj[id];
  }

  void removeRefContext(String refField, int id) {
    Map<int, SubscriptionQuery> byObj = refs[refField];
    if (byObj != null) {
      byObj.remove(id);
    }
  }

  SubscriptionHolder getInverseHolder(String field) {
    SubscriptionHolder inverseHolder = inverseHolders[field];
    if (inverseHolder == null) {
      inverseHolder = SubscriptionHolder(type);
      inverseHolders[field] = inverseHolder;
    }
    return inverseHolder;
  }

  void removeInverseHolder(field) {
    inverseHolders.remove(field);
  }

  void addQuery(SubscriptionQuery query, List<int> ids) {
    List<int> existing = queries[query];
    if (existing == null) {
      queries[query] = ids;
    } else {
      existing.addAll(ids);
    }
  }

  void remove(SubscriptionQuery query, List<int> ids) {
    List<int> existing = queries[query];
    if (existing == null) {
      return;
    }
    ids.forEach((element) {
      existing.remove(element);
    });
    if (existing.isEmpty) {
      queries.remove(query);
    }
  }

  bool prepareQuery(bool byId) {
    SubscriptionQuery combined = SubscriptionQuery();
    queries.keys.forEach((q) => merge(combined, q));
    if (combined.subTypes != null && byId) {
      combined.fields = combined.subTypes[type];
      combined.subTypes = null;
    }
    combined.creatable = false;
    String query = prepareQueryInternal(combined);
    String ids = combineIds();
    if (this.query == query && this.ids == ids) {
      return false;
    }
    this.ids = ids;
    this.query = query;
    return true;
  }

  String combineIds() {
    Set<int> allIds = {};
    queries.values.forEach((element) {
      allIds.addAll(element);
    });
    List<int> sorted = [...allIds];
    sorted.sort();
    return sorted.toString();
  }

  String prepareQueryInternal(SubscriptionQuery query) {
    if (query.subTypes != null) {
      StringBuffer q = StringBuffer();
      q.write('{');
      q.write(' __typename');
      query.subTypes.forEach((t, qv) {
        q.write(' ... on ');
        q.write(t);
        // if (query.creatable) {
        // q.write('{id}');
        // } else {
        q.write(prepareQueryFromMap(qv));
        // }
      });
      q.write('}');
      return q.toString();
    }
    if (query.fields != null) {
      // if (query.creatable) {
      // return '{id}';
      // }
      return prepareQueryFromMap(query.fields);
    }
    return '';
  }

  String prepareQueryId(SubscriptionQuery query) {
    if (query.subTypes != null) {
      StringBuffer q = StringBuffer();
      q.write('{');
      q.write(' __typename');
      query.subTypes.forEach((t, qv) {
        q.write(' ... on ');
        q.write(t);
        q.write('{id}');
      });
      q.write('}');
      return q.toString();
    }
    if (query.fields != null) {
      return '{id}';
    }
    return '';
  }

  String prepareQueryFromMap(Map<String, SubscriptionQuery> map) {
    List<String> keys = map.keys.toList()..sort();
    StringBuffer q = StringBuffer();
    q.write('{');
    keys.forEach((k) {
      SubscriptionQuery val = map[k];
      if (val != null && val.fields == null && val.subTypes == null) {
        return;
      }
      q.write(' ');
      if (val is SubscriptionQuery) {
        if (val.flat) {
          q.write(k + ' ' + prepareQueryId(val));
          q.write('flat' +
              k[0].toUpperCase() +
              k.substring(1) +
              ' ' +
              prepareQueryInternal(val));
        } else {
          q.write(k + ' ' + prepareQueryInternal(val));
        }
      } else {
        q.write(k);
      }
    });
    q.write('}');
    return q.toString();
  }

  void merge(SubscriptionQuery existing, SubscriptionQuery newQuery) {
    existing.creatable = newQuery.creatable;
    existing.flat = newQuery.flat;
    if (newQuery.subTypes != null) {
      if (existing.subTypes == null) {
        existing.subTypes = {};
      }
      newQuery.subTypes.forEach((t, q) {
        Map<String, SubscriptionQuery> existingQuery = existing.subTypes[t];
        if (existingQuery == null) {
          existingQuery = Map<String, SubscriptionQuery>();
          existing.subTypes[t] = existingQuery;
        }
        mergeFields(existingQuery, q);
      });
    } else if (newQuery.fields != null) {
      if (existing.fields == null) {
        existing.fields = Map<String, SubscriptionQuery>();
      }
      mergeFields(existing.fields, newQuery.fields);
    }
  }

  void mergeFields(Map<String, SubscriptionQuery> existing,
      Map<String, SubscriptionQuery> newQuery) {
    newQuery.forEach((field, query) {
      SubscriptionQuery val = existing[field];
      if (val == null) {
        if (query == null) {
          existing[field] = null;
        } else {
          existing[field] = SubscriptionQuery();
          merge(existing[field], query);
        }
        return;
      }

      if (query != null) {
        merge(val, query);
      }
    });
  }
}
