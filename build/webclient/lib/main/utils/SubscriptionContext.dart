import 'package:d3e_lang_dart/core.dart';
import 'package:d3e_studio/main/utils/DBObject.dart';

import '../classes/ObjectListSubscription.dart';

import 'BaseObject.dart';
import 'SubscriptionHolder.dart';
import 'SubscriptionQuery.dart';
import '../classes/Subscription.dart';

class SubscriptionContext {
  SubscriptionQuery query;
  Map<String, SubscriptionQuery> fields;
  BaseObject parent;
  bool isSubscription;
  Set<BaseObject> done;
  SubscriptionContext(this.query, this.isSubscription, this.done);

  bool has(String field) {
    return fields.containsKey(field);
  }

  void performInverse(DBObject on, String inverseSubscriptionName, String field,
      String type, BiConsumer<String, DBObject> onEvent) {
    if (!has(field)) {
      return;
    }
    SubscriptionContext subCtx =
        SubscriptionContext(fields[field], isSubscription, {});
    subCtx.parent = on;
    subCtx.selectFieldsFromSubType(type);
    Subscription sub = Subscription.get();
    if (isSubscription) {
      sub.subscribeInverse(
          on, inverseSubscriptionName, field, type, subCtx.query, (t, obj) {
        onEvent(t, obj);
        if (t == 'Insert') {
          subCtx.subscribeCollectionByType(obj.d3eType, [obj]);
        }
        if (t == 'Delete') {
          subCtx.unsubscribeCollectionByType(obj.d3eType, [obj]);
        }
      });
    } else {
      sub.unsubscribeInverse(on, field, subCtx.query);
    }
  }

  void performCollection(String field, Iterable<BaseObject> refs) {
    if (!has(field)) {
      return;
    }
    if (parent != null) {
      Subscription sub = Subscription.get();
      SubscriptionHolder parentHolder = sub.getHolder(parent.d3eType);
      if (isSubscription) {
        parentHolder.addRefContext(
            field, (parent as DBObject).id, fields[field]);
      } else {
        parentHolder.removeRefContext(field, (parent as DBObject).id);
      }
    }
    SubscriptionContext subCtx =
        SubscriptionContext(fields[field], isSubscription, {});
    Map<String, List<BaseObject>> byType = groupByType(refs);
    if (isSubscription) {
      byType.forEach((key, value) {
        subCtx.subscribeCollectionByType(key, value);
      });
    } else {
      byType.forEach((key, value) {
        subCtx.unsubscribeCollectionByType(key, value);
      });
    }
  }

  Map<String, List<BaseObject>> groupByType(Iterable<BaseObject> refs) {
    Map<String, List<BaseObject>> byType = {};
    refs.forEach((e) {
      List<BaseObject> list = byType[e.d3eType];
      if (list == null) {
        list = [];
        byType[e.d3eType] = list;
      }
      list.add(e);
    });
    return byType;
  }

  void perform(String field, BaseObject ref) {
    if (!has(field)) {
      return;
    }
    if (parent != null) {
      Subscription sub = Subscription.get();
      SubscriptionHolder parentHolder = sub.getHolder(parent.d3eType);
      if (isSubscription) {
        parentHolder.addRefContext(
            field, (parent as DBObject).id, fields[field]);
      } else {
        parentHolder.removeRefContext(field, (parent as DBObject).id);
      }
    }
    if (ref == null) {
      return null;
    }
    SubscriptionContext subCtx =
        SubscriptionContext(fields[field], isSubscription, {});
    if (isSubscription) {
      subCtx.subscribeCollectionByType(ref.d3eType, [ref]);
    } else {
      subCtx.unsubscribeCollectionByType(ref.d3eType, [ref]);
    }
  }

  void selectFieldsFromSubType(String type) {
    if (this.query.subTypes != null) {
      this.fields = this.query.subTypes[type];
    } else if (this.query.fields != null) {
      this.fields = query.fields;
    }
    if (this.fields == null) {
      this.fields = Map<String, SubscriptionQuery>();
    }
  }

  void subscribeCollectionByType(String type, List<BaseObject> objs) {
    selectFieldsFromSubType(type);
    if (!isValidQuery()) {
      return;
    }
    if (query.creatable) {
      Subscription sub = Subscription.get();
      sub.subscribeById(
          type, objs.map((o) => (o as DBObject).id).toList(), query);
    }
    objs.forEach((obj) {
      if (!done.contains(obj)) {
        done.add(obj);
        parent = obj;
        obj.subscribe(this);
      }
    });
  }

  void performStruct(Object struct) {
    if (!isSubscription) {
      ObjectListSubscription.get().unsubscribe(struct);
    }
  }

  void unsubscribeCollectionByType(String type, List<BaseObject> objs) {
    selectFieldsFromSubType(type);
    objs.forEach((obj) {
      if (!done.contains(obj)) {
        done.add(obj);
        parent = obj;
        obj.subscribe(this);
      }
    });
    if (query.creatable) {
      Subscription sub = Subscription.get();
      sub.unsubscribeById(
          type, objs.map((o) => (o as DBObject).id).toList(), query);
    }
  }

  bool isValidQuery() {
    if (fields.isEmpty) {
      return false;
    }
    if (fields.length == 1 && fields.containsKey('id')) {
      return false;
    }
    return true;
  }
}
