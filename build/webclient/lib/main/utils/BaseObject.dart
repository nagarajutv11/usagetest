import 'SubscriptionContext.dart';

abstract class BaseObject {
  String get d3eType;
  void subscribe(SubscriptionContext ctx);
}
