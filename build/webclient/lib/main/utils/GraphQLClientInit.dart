import 'package:d3e_studio/main/Config.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'LocalDataStore.dart';

class DummyCache implements Cache {
  dynamic read(String key) {
    return null;
  }

  void write(
    String key,
    dynamic value,
  ) {}

  Future<void> save() async {}

  void restore() {}

  void reset() {}
}

class GraphQLClientInit {
  static GraphQLClient _client;
  static String token;
  static GraphQLClient get() {
    if (_client == null) {
      final _httpLink = HttpLink(uri: Config.get().baseHttpUrl + '/graphql');
      final AuthLink _authLink = AuthLink(getToken: () async {
        if (token == null) {
          return '';
        }
        return 'Bearer $token';
      });
      final Link _link = _authLink.concat(_httpLink);
      _client = GraphQLClient(link: _link, cache: DummyCache());
    }
    return _client;
  }
}
