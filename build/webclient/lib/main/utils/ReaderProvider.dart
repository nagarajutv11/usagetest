import '../models/CForData.dart';
import '../models/CIfData.dart';
import '../models/CPopupData.dart';
import '../models/CRef.dart';
import '../models/CRefData.dart';
import '../models/CRefSlotData.dart';
import '../models/CSlotData.dart';
import '../models/CSwitchData.dart';
import '../models/ComponentTestData.dart';
import '../models/ModelTestData.dart';
import '../models/PropertyTestData.dart';
import '../models/PropertyValue.dart';
import '../models/StructTestData.dart';
import 'JsonReader.dart';

class ReaderProvider {
  Map<String, JsonReader> reader = {};
  static ReaderProvider ins;
  static ReaderProvider get() {
    if (ins != null) {
      return ins;
    }

    ins = ReaderProvider();

    ins.reader['CForData'] = CForDataReader();

    ins.reader['CIfData'] = CIfDataReader();

    ins.reader['CPopupData'] = CPopupDataReader();

    ins.reader['CRef'] = CRefReader();

    ins.reader['CRefData'] = CRefDataReader();

    ins.reader['CRefSlotData'] = CRefSlotDataReader();

    ins.reader['CSlotData'] = CSlotDataReader();

    ins.reader['CSwitchData'] = CSwitchDataReader();

    ins.reader['ComponentTestData'] = ComponentTestDataReader();

    ins.reader['ModelTestData'] = ModelTestDataReader();

    ins.reader['PropertyTestData'] = PropertyTestDataReader();

    ins.reader['PropertyValue'] = PropertyValueReader();

    ins.reader['StructTestData'] = StructTestDataReader();

    return ins;
  }

  JsonReader getReader(String type) {
    return reader[type];
  }
}
