import 'main/Config.dart';
import 'package:flutter/material.dart';
import 'package:d3e_studio/MyApp.dart';
import 'package:global_configuration/global_configuration.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  (await GlobalConfiguration().loadFromPath('resource/settings.json'));

  Config.get().load(GlobalConfiguration());

  runApp(MyApp());
}
