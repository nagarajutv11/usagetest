package d3e.core;

import classes.LoginResult;
import java.util.Optional;
import javax.annotation.PostConstruct;
import models.AnonymousUser;
import models.ComponentTestData;
import models.EmailMessage;
import models.ModelTestData;
import models.OneTimePassword;
import models.Test;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.AnonymousUserRepository;
import repository.jpa.AvatarRepository;
import repository.jpa.CForDataRepository;
import repository.jpa.CIfDataRepository;
import repository.jpa.CNodeDataRepository;
import repository.jpa.CPopupDataRepository;
import repository.jpa.CRefDataRepository;
import repository.jpa.CRefRepository;
import repository.jpa.CRefSlotDataRepository;
import repository.jpa.CSlotDataRepository;
import repository.jpa.CSwitchDataRepository;
import repository.jpa.ComponentTestDataRepository;
import repository.jpa.EmailMessageRepository;
import repository.jpa.ModelTestDataRepository;
import repository.jpa.OneTimePasswordRepository;
import repository.jpa.PropertyTestDataRepository;
import repository.jpa.PropertyValueRepository;
import repository.jpa.ReportConfigOptionRepository;
import repository.jpa.ReportConfigRepository;
import repository.jpa.StructTestDataRepository;
import repository.jpa.TestRepository;
import repository.jpa.UserRepository;
import repository.jpa.UserSessionRepository;
import security.AppSessionProvider;
import security.JwtTokenUtil;

@Service
public class QueryProvider {
  public static QueryProvider instance;
  @Autowired private JwtTokenUtil jwtTokenUtil;
  @Autowired private AnonymousUserRepository anonymousUserRepository;
  @Autowired private AvatarRepository avatarRepository;
  @Autowired private EmailMessageRepository emailMessageRepository;
  @Autowired private OneTimePasswordRepository oneTimePasswordRepository;
  @Autowired private ReportConfigRepository reportConfigRepository;
  @Autowired private ReportConfigOptionRepository reportConfigOptionRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private UserSessionRepository userSessionRepository;
  @Autowired private CRefRepository cRefRepository;
  @Autowired private ModelTestDataRepository modelTestDataRepository;
  @Autowired private ComponentTestDataRepository componentTestDataRepository;
  @Autowired private StructTestDataRepository structTestDataRepository;
  @Autowired private CNodeDataRepository cNodeDataRepository;
  @Autowired private CForDataRepository cForDataRepository;
  @Autowired private CIfDataRepository cIfDataRepository;
  @Autowired private CPopupDataRepository cPopupDataRepository;
  @Autowired private CRefDataRepository cRefDataRepository;
  @Autowired private CRefSlotDataRepository cRefSlotDataRepository;
  @Autowired private CSlotDataRepository cSlotDataRepository;
  @Autowired private CSwitchDataRepository cSwitchDataRepository;
  @Autowired private PropertyTestDataRepository propertyTestDataRepository;
  @Autowired private TestRepository testRepository;
  @Autowired private PropertyValueRepository propertyValueRepository;
  @Autowired private ObjectFactory<AppSessionProvider> provider;

  @PostConstruct
  public void init() {
    instance = this;
  }

  public static QueryProvider get() {
    return instance;
  }

  public AnonymousUser getAnonymousUserById(long id) {
    Optional<AnonymousUser> findById = anonymousUserRepository.findById(id);
    return findById.orElse(null);
  }

  public EmailMessage getEmailMessageById(long id) {
    Optional<EmailMessage> findById = emailMessageRepository.findById(id);
    return findById.orElse(null);
  }

  public OneTimePassword getOneTimePasswordById(long id) {
    Optional<OneTimePassword> findById = oneTimePasswordRepository.findById(id);
    return findById.orElse(null);
  }

  public Boolean checkOneTimePasswordTokenUnique(long id, String name) {
    return !(oneTimePasswordRepository.isUniqueToken(id, name));
  }

  public ModelTestData getModelTestDataById(long id) {
    Optional<ModelTestData> findById = modelTestDataRepository.findById(id);
    return findById.orElse(null);
  }

  public ComponentTestData getComponentTestDataById(long id) {
    Optional<ComponentTestData> findById = componentTestDataRepository.findById(id);
    return findById.orElse(null);
  }

  public Test getTestById(long id) {
    Optional<Test> findById = testRepository.findById(id);
    return findById.orElse(null);
  }

  public LoginResult loginWithOTP(String token, String code) {
    OneTimePassword otp = oneTimePasswordRepository.getByToken(token);
    LoginResult loginResult = new LoginResult();
    loginResult.success = true;
    loginResult.userObject = otp.getUser();
    loginResult.token = token;
    return loginResult;
  }
}
