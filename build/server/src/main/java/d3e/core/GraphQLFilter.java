package d3e.core;

import javax.transaction.Transactional;

public class GraphQLFilter extends org.springframework.web.filter.OncePerRequestFilter {

	private TransactionWrapper wrapper;

	public GraphQLFilter(TransactionWrapper wrapper) {
		this.wrapper = wrapper;
	}

	@java.lang.Override
	@Transactional
	protected void doFilterInternal(javax.servlet.http.HttpServletRequest request,
			javax.servlet.http.HttpServletResponse response, javax.servlet.FilterChain filterChain)
			throws javax.servlet.ServletException, java.io.IOException {
		wrapper.doInTransaction(() -> filterChain.doFilter(request, response));
	}

	@Override
	protected boolean shouldNotFilterAsyncDispatch() {
		return false;
	}
}
