package d3e.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileController {
	private static final String TEMP_PREFIX = "temp:";
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@PostMapping("/upload")
	public DFile uploadFile(@RequestParam("file") MultipartFile multiFile) {
		DFile file = storeFile(multiFile);
		return file;
	}

	DFile storeFile(MultipartFile multiFile) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(multiFile.getOriginalFilename());
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new RuntimeException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			File targetLocation = File.createTempFile(UUID.randomUUID().toString(), ".d3e");
			Files.copy(multiFile.getInputStream(), targetLocation.toPath(), StandardCopyOption.REPLACE_EXISTING);
			DFile dfile = new DFile();
			dfile.setId(TEMP_PREFIX + targetLocation.getName());
			dfile.setName(fileName);
			dfile.setSize(targetLocation.length());
			return dfile;
		} catch (IOException ex) {
			throw new RuntimeException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	@PostMapping("/uploads")
	public List<DFile> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
		return Arrays.asList(files).stream().map(file -> uploadFile(file)).collect(Collectors.toList());
	}

	@GetMapping("/download/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	public static Resource loadFileAsResource(String fileName) {
		Path filePath;
		if (fileName.startsWith(TEMP_PREFIX)) {
			fileName = fileName.substring(TEMP_PREFIX.length());
			String tempPath = System.getProperty("java.io.tmpdir");
			File targetLocation = new File(tempPath + File.separator + fileName);
			filePath = targetLocation.toPath();
		} else {
			throw new RuntimeException("File not found " + fileName);
		}
		try {
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new RuntimeException("File not found " + fileName);
			}
		} catch (Exception e) {
			throw new RuntimeException("File not found " + fileName);
		}
	}
}