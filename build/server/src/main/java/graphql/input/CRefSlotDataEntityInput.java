package graphql.input;

import java.util.List;

public class CRefSlotDataEntityInput implements store.IEntityInput {
  private long id;
  private String slot;
  private CNodeDataEntityInputUnionX child;
  private List<CNodeDataEntityInputUnionX> children;

  public String _type() {
    return "CRefSlotData";
  }

  public String getSlot() {
    return this.slot;
  }

  public void setSlot(String slot) {
    this.slot = slot;
  }

  public CNodeDataEntityInputUnionX getChild() {
    return this.child;
  }

  public void setChild(CNodeDataEntityInputUnionX child) {
    this.child = child;
  }

  public List<CNodeDataEntityInputUnionX> getChildren() {
    return this.children;
  }

  public void setChildren(List<CNodeDataEntityInputUnionX> children) {
    this.children = children;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
