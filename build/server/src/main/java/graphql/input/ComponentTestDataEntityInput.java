package graphql.input;

import java.util.List;

public class ComponentTestDataEntityInput implements store.IEntityInput {
  private long id;
  private String identity;
  private List<PropertyTestDataEntityInput> data;
  private List<StructTestDataEntityInput> structTempData;
  private CNodeDataEntityInputUnionX buildData;

  public String _type() {
    return "ComponentTestData";
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<PropertyTestDataEntityInput> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestDataEntityInput> data) {
    this.data = data;
  }

  public List<StructTestDataEntityInput> getStructTempData() {
    return this.structTempData;
  }

  public void setStructTempData(List<StructTestDataEntityInput> structTempData) {
    this.structTempData = structTempData;
  }

  public CNodeDataEntityInputUnionX getBuildData() {
    return this.buildData;
  }

  public void setBuildData(CNodeDataEntityInputUnionX buildData) {
    this.buildData = buildData;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
