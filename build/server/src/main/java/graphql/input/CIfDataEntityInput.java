package graphql.input;

public class CIfDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private boolean condition;
  private CNodeDataEntityInputUnionX value;

  public String _type() {
    return "CIfData";
  }

  public boolean isCondition() {
    return this.condition;
  }

  public void setCondition(boolean condition) {
    this.condition = condition;
  }

  public CNodeDataEntityInputUnionX getValue() {
    return this.value;
  }

  public void setValue(CNodeDataEntityInputUnionX value) {
    this.value = value;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
