package graphql.input;

import java.util.List;

public class CForDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private List<PropertyTestDataEntityInput> data;
  private List<CNodeDataEntityInputUnionX> items;

  public String _type() {
    return "CForData";
  }

  public List<PropertyTestDataEntityInput> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestDataEntityInput> data) {
    this.data = data;
  }

  public List<CNodeDataEntityInputUnionX> getItems() {
    return this.items;
  }

  public void setItems(List<CNodeDataEntityInputUnionX> items) {
    this.items = items;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
