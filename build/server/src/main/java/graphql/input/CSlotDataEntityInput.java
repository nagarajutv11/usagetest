package graphql.input;

public class CSlotDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private String slot;
  private CRefSlotDataEntityInput actual;

  public String _type() {
    return "CSlotData";
  }

  public String getSlot() {
    return this.slot;
  }

  public void setSlot(String slot) {
    this.slot = slot;
  }

  public CRefSlotDataEntityInput getActual() {
    return this.actual;
  }

  public void setActual(CRefSlotDataEntityInput actual) {
    this.actual = actual;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
