package graphql.input;

import java.util.List;

public class PropertyTestDataEntityInput implements store.IEntityInput {
  private long id;
  private String identity;
  private List<String> primitiveValue;
  private List<ObjectRef> modelValue;
  private List<ObjectRef> structValue;

  public String _type() {
    return "PropertyTestData";
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<String> getPrimitiveValue() {
    return this.primitiveValue;
  }

  public void setPrimitiveValue(List<String> primitiveValue) {
    this.primitiveValue = primitiveValue;
  }

  public List<ObjectRef> getModelValue() {
    return this.modelValue;
  }

  public void setModelValue(List<ObjectRef> modelValue) {
    this.modelValue = modelValue;
  }

  public List<ObjectRef> getStructValue() {
    return this.structValue;
  }

  public void setStructValue(List<ObjectRef> structValue) {
    this.structValue = structValue;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
