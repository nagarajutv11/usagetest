package graphql.input;

import d3e.core.DFile;

public class D3EImageEntityInput implements store.IEntityInput {
  private long id;
  private long size;
  private long width;
  private long height;
  private DFile file;

  public String _type() {
    return "D3EImage";
  }

  public long getSize() {
    return this.size;
  }

  public void setSize(long size) {
    this.size = size;
  }

  public long getWidth() {
    return this.width;
  }

  public void setWidth(long width) {
    this.width = width;
  }

  public long getHeight() {
    return this.height;
  }

  public void setHeight(long height) {
    this.height = height;
  }

  public DFile getFile() {
    return this.file;
  }

  public void setFile(DFile file) {
    this.file = file;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
