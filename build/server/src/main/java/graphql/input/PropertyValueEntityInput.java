package graphql.input;

public class PropertyValueEntityInput implements store.IEntityInput {
  private long id;
  private String thenValue;
  private String elseValue;
  private String defaultValue;
  private String value;
  private String fromTheme;

  public String _type() {
    return "PropertyValue";
  }

  public String getThenValue() {
    return this.thenValue;
  }

  public void setThenValue(String thenValue) {
    this.thenValue = thenValue;
  }

  public String getElseValue() {
    return this.elseValue;
  }

  public void setElseValue(String elseValue) {
    this.elseValue = elseValue;
  }

  public String getDefaultValue() {
    return this.defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getFromTheme() {
    return this.fromTheme;
  }

  public void setFromTheme(String fromTheme) {
    this.fromTheme = fromTheme;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
