package graphql.input;

import java.util.List;

public class CRefDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private List<PropertyTestDataEntityInput> data;
  private CNodeDataEntityInputUnionX builder;
  private CNodeDataEntityInputUnionX buildData;
  private CNodeDataEntityInputUnionX child;
  private List<CNodeDataEntityInputUnionX> children;
  private List<CRefSlotDataEntityInput> slots;

  public String _type() {
    return "CRefData";
  }

  public List<PropertyTestDataEntityInput> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestDataEntityInput> data) {
    this.data = data;
  }

  public CNodeDataEntityInputUnionX getBuilder() {
    return this.builder;
  }

  public void setBuilder(CNodeDataEntityInputUnionX builder) {
    this.builder = builder;
  }

  public CNodeDataEntityInputUnionX getBuildData() {
    return this.buildData;
  }

  public void setBuildData(CNodeDataEntityInputUnionX buildData) {
    this.buildData = buildData;
  }

  public CNodeDataEntityInputUnionX getChild() {
    return this.child;
  }

  public void setChild(CNodeDataEntityInputUnionX child) {
    this.child = child;
  }

  public List<CNodeDataEntityInputUnionX> getChildren() {
    return this.children;
  }

  public void setChildren(List<CNodeDataEntityInputUnionX> children) {
    this.children = children;
  }

  public List<CRefSlotDataEntityInput> getSlots() {
    return this.slots;
  }

  public void setSlots(List<CRefSlotDataEntityInput> slots) {
    this.slots = slots;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
