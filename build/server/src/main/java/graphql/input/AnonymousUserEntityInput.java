package graphql.input;

public class AnonymousUserEntityInput extends UserEntityInput {
  private long id;

  public String _type() {
    return "AnonymousUser";
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
