package graphql.input;

public class CNodeDataEntityInputUnionX implements store.IEntityInput {
  private String type;
  private CForDataEntityInput valueCForData;
  private CIfDataEntityInput valueCIfData;
  private CPopupDataEntityInput valueCPopupData;
  private CRefDataEntityInput valueCRefData;
  private CSlotDataEntityInput valueCSlotData;
  private CSwitchDataEntityInput valueCSwitchData;

  public String _type() {
    return type;
  }

  public long getId() {
    return 0l;
  }

  public void setType(String type) {
    this.type = type;
  }

  public CForDataEntityInput getValueCForData() {
    return this.valueCForData;
  }

  public void setValueCForData(CForDataEntityInput valueCForData) {
    this.valueCForData = valueCForData;
  }

  public CIfDataEntityInput getValueCIfData() {
    return this.valueCIfData;
  }

  public void setValueCIfData(CIfDataEntityInput valueCIfData) {
    this.valueCIfData = valueCIfData;
  }

  public CPopupDataEntityInput getValueCPopupData() {
    return this.valueCPopupData;
  }

  public void setValueCPopupData(CPopupDataEntityInput valueCPopupData) {
    this.valueCPopupData = valueCPopupData;
  }

  public CRefDataEntityInput getValueCRefData() {
    return this.valueCRefData;
  }

  public void setValueCRefData(CRefDataEntityInput valueCRefData) {
    this.valueCRefData = valueCRefData;
  }

  public CSlotDataEntityInput getValueCSlotData() {
    return this.valueCSlotData;
  }

  public void setValueCSlotData(CSlotDataEntityInput valueCSlotData) {
    this.valueCSlotData = valueCSlotData;
  }

  public CSwitchDataEntityInput getValueCSwitchData() {
    return this.valueCSwitchData;
  }

  public void setValueCSwitchData(CSwitchDataEntityInput valueCSwitchData) {
    this.valueCSwitchData = valueCSwitchData;
  }

  public CNodeDataEntityInput getValue() {
    switch (type) {
      case "CForData":
        {
          return this.valueCForData;
        }
      case "CIfData":
        {
          return this.valueCIfData;
        }
      case "CPopupData":
        {
          return this.valueCPopupData;
        }
      case "CRefData":
        {
          return this.valueCRefData;
        }
      case "CSlotData":
        {
          return this.valueCSlotData;
        }
      case "CSwitchData":
        {
          return this.valueCSwitchData;
        }
      default:
        {
          return null;
        }
    }
  }
}
