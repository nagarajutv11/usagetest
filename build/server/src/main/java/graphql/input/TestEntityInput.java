package graphql.input;

import java.util.List;

public class TestEntityInput implements store.IEntityInput {
  private long id;
  private List<ObjectRef> childrens;

  public String _type() {
    return "Test";
  }

  public List<ObjectRef> getChildrens() {
    return this.childrens;
  }

  public void setChildrens(List<ObjectRef> childrens) {
    this.childrens = childrens;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
