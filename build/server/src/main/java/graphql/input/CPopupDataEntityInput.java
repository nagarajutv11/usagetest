package graphql.input;

public class CPopupDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private boolean showOn;
  private CNodeDataEntityInputUnionX body;
  private CNodeDataEntityInputUnionX placeHolder;

  public String _type() {
    return "CPopupData";
  }

  public boolean isShowOn() {
    return this.showOn;
  }

  public void setShowOn(boolean showOn) {
    this.showOn = showOn;
  }

  public CNodeDataEntityInputUnionX getBody() {
    return this.body;
  }

  public void setBody(CNodeDataEntityInputUnionX body) {
    this.body = body;
  }

  public CNodeDataEntityInputUnionX getPlaceHolder() {
    return this.placeHolder;
  }

  public void setPlaceHolder(CNodeDataEntityInputUnionX placeHolder) {
    this.placeHolder = placeHolder;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
