package graphql.input;

public class CSwitchDataEntityInput extends CNodeDataEntityInput {
  private long id;
  private long index;
  private PropertyTestDataEntityInput onValue;
  private CNodeDataEntityInputUnionX caseData;

  public String _type() {
    return "CSwitchData";
  }

  public long getIndex() {
    return this.index;
  }

  public void setIndex(long index) {
    this.index = index;
  }

  public PropertyTestDataEntityInput getOnValue() {
    return this.onValue;
  }

  public void setOnValue(PropertyTestDataEntityInput onValue) {
    this.onValue = onValue;
  }

  public CNodeDataEntityInputUnionX getCaseData() {
    return this.caseData;
  }

  public void setCaseData(CNodeDataEntityInputUnionX caseData) {
    this.caseData = caseData;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
