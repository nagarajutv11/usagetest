package graphql.input;

import java.util.List;

public class CRefEntityInput implements store.IEntityInput {
  private long id;
  private List<PropertyValueEntityInput> data;

  public String _type() {
    return "CRef";
  }

  public List<PropertyValueEntityInput> getData() {
    return this.data;
  }

  public void setData(List<PropertyValueEntityInput> data) {
    this.data = data;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
