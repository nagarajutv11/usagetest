package graphql.input;

import java.util.List;

public class StructTestDataEntityInput implements store.IEntityInput {
  private long id;
  private String identity;
  private List<PropertyTestDataEntityInput> data;

  public String _type() {
    return "StructTestData";
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<PropertyTestDataEntityInput> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestDataEntityInput> data) {
    this.data = data;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
