package graphql;

import classes.LoginResult;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.schema.DataFetchingEnvironment;
import java.util.Optional;
import models.AnonymousUser;
import models.ComponentTestData;
import models.EmailMessage;
import models.ModelTestData;
import models.OneTimePassword;
import models.Test;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import repository.jpa.AnonymousUserRepository;
import repository.jpa.ComponentTestDataRepository;
import repository.jpa.EmailMessageRepository;
import repository.jpa.ModelTestDataRepository;
import repository.jpa.OneTimePasswordRepository;
import repository.jpa.TestRepository;
import repository.jpa.UserRepository;
import repository.jpa.UserSessionRepository;
import security.AppSessionProvider;
import security.JwtTokenUtil;

@org.springframework.stereotype.Component
public class Query implements GraphQLQueryResolver {
  @Autowired private JwtTokenUtil jwtTokenUtil;
  @Autowired private PasswordEncoder passwordEncoder;
  @Autowired private AnonymousUserRepository anonymousUserRepository;
  @Autowired private EmailMessageRepository emailMessageRepository;
  @Autowired private OneTimePasswordRepository oneTimePasswordRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private UserSessionRepository userSessionRepository;
  @Autowired private ModelTestDataRepository modelTestDataRepository;
  @Autowired private ComponentTestDataRepository componentTestDataRepository;
  @Autowired private TestRepository testRepository;
  @Autowired private ObjectFactory<AppSessionProvider> provider;

  public AnonymousUser getAnonymousUserById(long id, DataFetchingEnvironment env) {
    Optional<AnonymousUser> findById = anonymousUserRepository.findById(id);
    return findById.orElse(null);
  }

  public EmailMessage getEmailMessageById(long id, DataFetchingEnvironment env) {
    Optional<EmailMessage> findById = emailMessageRepository.findById(id);
    return findById.orElse(null);
  }

  public OneTimePassword getOneTimePasswordById(long id, DataFetchingEnvironment env) {
    Optional<OneTimePassword> findById = oneTimePasswordRepository.findById(id);
    return findById.orElse(null);
  }

  public Boolean checkOneTimePasswordTokenUnique(long id, String name) {
    return !(oneTimePasswordRepository.isUniqueToken(id, name));
  }

  public ModelTestData getModelTestDataById(long id, DataFetchingEnvironment env) {
    Optional<ModelTestData> findById = modelTestDataRepository.findById(id);
    return findById.orElse(null);
  }

  public ComponentTestData getComponentTestDataById(long id, DataFetchingEnvironment env) {
    Optional<ComponentTestData> findById = componentTestDataRepository.findById(id);
    return findById.orElse(null);
  }

  public Test getTestById(long id, DataFetchingEnvironment env) {
    Optional<Test> findById = testRepository.findById(id);
    return findById.orElse(null);
  }

  public LoginResult loginWithOTP(String token, String code) {
    OneTimePassword otp = oneTimePasswordRepository.getByToken(token);
    LoginResult loginResult = new classes.LoginResult();
    if (otp == null) {
      loginResult.success = false;
      loginResult.failureMessage = "Invalid token.";
      return loginResult;
    }
    if (!(code.equals(otp.getCode()))) {
      loginResult.success = false;
      loginResult.failureMessage = "Invalid code.";
      return loginResult;
    }
    loginResult.success = true;
    loginResult.userObject = otp.getUser();
    loginResult.token = token;
    return loginResult;
  }
}
