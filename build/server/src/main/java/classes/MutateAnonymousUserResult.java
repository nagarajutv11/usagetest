package classes;

import java.util.List;
import models.AnonymousUser;

public class MutateAnonymousUserResult extends MutateResult<AnonymousUser> {
  public MutateAnonymousUserResult() {}

  public MutateAnonymousUserResult(Boolean success, AnonymousUser value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
