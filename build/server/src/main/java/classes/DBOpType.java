package classes;

public enum DBOpType {
  CREATE,
  UPDATE,
  DELETE;
}
