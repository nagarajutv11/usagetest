package classes;

import java.util.List;
import models.EmailMessage;

public class MutateEmailMessageResult extends MutateResult<EmailMessage> {
  public MutateEmailMessageResult() {}

  public MutateEmailMessageResult(Boolean success, EmailMessage value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
