package classes;

import java.util.List;
import models.OneTimePassword;

public class MutateOneTimePasswordResult extends MutateResult<OneTimePassword> {
  public MutateOneTimePasswordResult() {}

  public MutateOneTimePasswordResult(Boolean success, OneTimePassword value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
