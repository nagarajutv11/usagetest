package classes;

import java.util.List;
import models.Test;

public class MutateTestResult extends MutateResult<Test> {
  public MutateTestResult() {}

  public MutateTestResult(Boolean success, Test value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
