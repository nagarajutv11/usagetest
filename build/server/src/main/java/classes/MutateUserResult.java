package classes;

import java.util.List;
import models.User;

public class MutateUserResult extends MutateResult<User> {
  public MutateUserResult() {}

  public MutateUserResult(Boolean success, User value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
