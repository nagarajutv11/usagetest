package classes;

public class DBOperation {
  public DBOpType type;
  public Object data;

  public DBOperation(DBOpType type, Object data) {
    this.type = type;
    this.data = data;
  }

  public DBOpType getType() {
    return this.type;
  }

  public Object getData() {
    return this.data;
  }
}
