package classes;

import java.util.List;

public class MutateResult<T> {
  public Boolean success;
  public T value;
  public List<String> errors;
}
