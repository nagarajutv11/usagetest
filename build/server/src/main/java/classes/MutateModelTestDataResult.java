package classes;

import java.util.List;
import models.ModelTestData;

public class MutateModelTestDataResult extends MutateResult<ModelTestData> {
  public MutateModelTestDataResult() {}

  public MutateModelTestDataResult(Boolean success, ModelTestData value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
