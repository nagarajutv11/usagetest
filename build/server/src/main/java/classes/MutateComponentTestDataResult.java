package classes;

import java.util.List;
import models.ComponentTestData;

public class MutateComponentTestDataResult extends MutateResult<ComponentTestData> {
  public MutateComponentTestDataResult() {}

  public MutateComponentTestDataResult(
      Boolean success, ComponentTestData value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
