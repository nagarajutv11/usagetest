package classes;

import java.util.List;
import models.UserSession;

public class MutateUserSessionResult extends MutateResult<UserSession> {
  public MutateUserSessionResult() {}

  public MutateUserSessionResult(Boolean success, UserSession value, List<String> errors) {
    this.success = success;
    this.value = value;
    this.errors = errors;
  }
}
