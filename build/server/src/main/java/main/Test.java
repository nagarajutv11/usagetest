package main;

import com.coxautodev.graphql.tools.SchemaParserDictionary;
import d3e.core.GraphQLFilter;
import d3e.core.TransactionWrapper;
import graphql.events.AnonymousUserChangeEvent;
import graphql.events.ComponentTestDataChangeEvent;
import graphql.events.EmailMessageChangeEvent;
import graphql.events.ModelTestDataChangeEvent;
import graphql.events.OneTimePasswordChangeEvent;
import graphql.events.TestChangeEvent;
import graphql.events.UserChangeEvent;
import graphql.events.UserSessionChangeEvent;
import graphql.input.AnonymousUserEntityInput;
import graphql.input.CForDataEntityInput;
import graphql.input.CIfDataEntityInput;
import graphql.input.CPopupDataEntityInput;
import graphql.input.CRefDataEntityInput;
import graphql.input.CSlotDataEntityInput;
import graphql.input.CSwitchDataEntityInput;
import models.AnonymousUser;
import models.CForData;
import models.CIfData;
import models.CPopupData;
import models.CRefData;
import models.CSlotData;
import models.CSwitchData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

@SpringBootApplication
@EnableJpaAuditing
@EnableTransactionManagement
@ComponentScan({
  "test",
  "classes",
  "graphql",
  "helpers",
  "models",
  "repository",
  "security",
  "storage",
  "d3e.core",
  "store",
  "parser"
})
@EntityScan({
  "test",
  "classes",
  "graphql",
  "helpers",
  "models",
  "repository",
  "security",
  "storage",
  "d3e.core",
  "org.meta"
})
@EnableJpaRepositories("repository.jpa")
@EnableSolrRepositories("repository.solr")
public class Test {
  public static void main(String[] args) {
    SpringApplication.run(Test.class, args);
  }

  @Bean
  public FilterRegistrationBean<GraphQLFilter> graphQLFilter(TransactionWrapper wrapper) {
    FilterRegistrationBean<GraphQLFilter> registrationBean =
        new FilterRegistrationBean<GraphQLFilter>();
    registrationBean.setFilter(new GraphQLFilter(wrapper));
    registrationBean.addUrlPatterns("/graphql");
    return registrationBean;
  }

  @Bean
  public SchemaParserDictionary getSchemaParser() {
    SchemaParserDictionary dictionary = new SchemaParserDictionary();
    dictionary.add("AnonymousUserChangeEvent", AnonymousUserChangeEvent.class);
    dictionary.add("ComponentTestDataChangeEvent", ComponentTestDataChangeEvent.class);
    dictionary.add("EmailMessageChangeEvent", EmailMessageChangeEvent.class);
    dictionary.add("ModelTestDataChangeEvent", ModelTestDataChangeEvent.class);
    dictionary.add("OneTimePasswordChangeEvent", OneTimePasswordChangeEvent.class);
    dictionary.add("TestChangeEvent", TestChangeEvent.class);
    dictionary.add("UserChangeEvent", UserChangeEvent.class);
    dictionary.add("UserSessionChangeEvent", UserSessionChangeEvent.class);
    dictionary.add("AnonymousUser", AnonymousUser.class);
    dictionary.add("CForData", CForData.class);
    dictionary.add("CIfData", CIfData.class);
    dictionary.add("CPopupData", CPopupData.class);
    dictionary.add("CRefData", CRefData.class);
    dictionary.add("CSlotData", CSlotData.class);
    dictionary.add("CSwitchData", CSwitchData.class);
    dictionary.add("AnonymousUserEntityInput", AnonymousUserEntityInput.class);
    dictionary.add("CForDataEntityInput", CForDataEntityInput.class);
    dictionary.add("CIfDataEntityInput", CIfDataEntityInput.class);
    dictionary.add("CPopupDataEntityInput", CPopupDataEntityInput.class);
    dictionary.add("CRefDataEntityInput", CRefDataEntityInput.class);
    dictionary.add("CSlotDataEntityInput", CSlotDataEntityInput.class);
    dictionary.add("CSwitchDataEntityInput", CSwitchDataEntityInput.class);
    return dictionary;
  }

  @Bean
  public ServletServerContainerFactoryBean createServletServerContainerFactoryBean() {
    ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
    container.setMaxTextMessageBufferSize(32768);
    container.setMaxBinaryMessageBufferSize(32768);
    return container;
  }
}
