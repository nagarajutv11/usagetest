package repository.solr;

@org.springframework.stereotype.Repository
public interface TestSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.Test, Long> {}
