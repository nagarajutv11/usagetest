package repository.solr;

@org.springframework.stereotype.Repository
public interface StructTestDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.StructTestData, Long> {}
