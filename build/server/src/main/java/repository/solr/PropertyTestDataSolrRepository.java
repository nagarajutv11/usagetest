package repository.solr;

@org.springframework.stereotype.Repository
public interface PropertyTestDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.PropertyTestData, Long> {}
