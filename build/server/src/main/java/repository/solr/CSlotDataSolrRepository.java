package repository.solr;

@org.springframework.stereotype.Repository
public interface CSlotDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CSlotData, Long> {}
