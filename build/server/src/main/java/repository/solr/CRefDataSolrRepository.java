package repository.solr;

@org.springframework.stereotype.Repository
public interface CRefDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CRefData, Long> {}
