package repository.solr;

@org.springframework.stereotype.Repository
public interface ComponentTestDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.ComponentTestData, Long> {}
