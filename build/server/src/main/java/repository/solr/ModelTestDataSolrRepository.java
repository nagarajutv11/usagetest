package repository.solr;

@org.springframework.stereotype.Repository
public interface ModelTestDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.ModelTestData, Long> {}
