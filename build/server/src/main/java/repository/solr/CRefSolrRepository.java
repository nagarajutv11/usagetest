package repository.solr;

@org.springframework.stereotype.Repository
public interface CRefSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CRef, Long> {}
