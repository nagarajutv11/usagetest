package repository.solr;

@org.springframework.stereotype.Repository
public interface CRefSlotDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.CRefSlotData, Long> {}
