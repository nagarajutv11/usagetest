package repository.solr;

@org.springframework.stereotype.Repository
public interface CSwitchDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CSwitchData, Long> {}
