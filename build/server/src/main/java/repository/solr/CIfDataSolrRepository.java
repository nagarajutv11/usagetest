package repository.solr;

@org.springframework.stereotype.Repository
public interface CIfDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CIfData, Long> {}
