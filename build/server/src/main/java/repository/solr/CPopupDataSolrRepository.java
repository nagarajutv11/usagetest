package repository.solr;

@org.springframework.stereotype.Repository
public interface CPopupDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CPopupData, Long> {}
