package repository.solr;

@org.springframework.stereotype.Repository
public interface CNodeDataSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<models.CNodeData, Long> {}
