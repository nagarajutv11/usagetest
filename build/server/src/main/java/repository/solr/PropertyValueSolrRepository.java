package repository.solr;

@org.springframework.stereotype.Repository
public interface PropertyValueSolrRepository
    extends org.springframework.data.solr.repository.SolrCrudRepository<
        models.PropertyValue, Long> {}
