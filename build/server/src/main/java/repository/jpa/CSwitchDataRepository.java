package repository.jpa;

import models.CSwitchData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CSwitchDataRepository extends JpaRepository<CSwitchData, Long> {}
