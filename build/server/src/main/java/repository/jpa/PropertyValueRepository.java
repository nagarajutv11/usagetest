package repository.jpa;

import models.PropertyValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyValueRepository extends JpaRepository<PropertyValue, Long> {}
