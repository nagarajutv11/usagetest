package repository.jpa;

import models.StructTestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StructTestDataRepository extends JpaRepository<StructTestData, Long> {}
