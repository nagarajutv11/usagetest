package repository.jpa;

import models.CPopupData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CPopupDataRepository extends JpaRepository<CPopupData, Long> {}
