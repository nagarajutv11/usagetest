package repository.jpa;

import java.util.List;
import models.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {
  @Query("From models.Test test where :childrens member test.childrens")
  public List<Test> findByChildrens(Test childrens);
}
