package repository.jpa;

import models.CRefSlotData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CRefSlotDataRepository extends JpaRepository<CRefSlotData, Long> {}
