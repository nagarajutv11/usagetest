package repository.jpa;

import models.CRef;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CRefRepository extends JpaRepository<CRef, Long> {}
