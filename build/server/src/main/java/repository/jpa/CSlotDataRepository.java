package repository.jpa;

import models.CSlotData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CSlotDataRepository extends JpaRepository<CSlotData, Long> {}
