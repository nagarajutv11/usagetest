package repository.jpa;

import models.ComponentTestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentTestDataRepository extends JpaRepository<ComponentTestData, Long> {}
