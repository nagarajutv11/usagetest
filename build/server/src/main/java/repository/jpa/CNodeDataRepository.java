package repository.jpa;

import models.CNodeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CNodeDataRepository extends JpaRepository<CNodeData, Long> {}
