package repository.jpa;

import java.util.List;
import models.ModelTestData;
import models.PropertyTestData;
import models.StructTestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyTestDataRepository extends JpaRepository<PropertyTestData, Long> {
  @Query(
      "From models.PropertyTestData propertyTestData where :modelValue member propertyTestData.modelValue")
  public List<PropertyTestData> findByModelValue(ModelTestData modelValue);

  @Query(
      "From models.PropertyTestData propertyTestData where :structValue member propertyTestData.structValue")
  public List<PropertyTestData> findByStructValue(StructTestData structValue);
}
