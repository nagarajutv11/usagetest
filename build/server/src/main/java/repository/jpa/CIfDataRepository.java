package repository.jpa;

import models.CIfData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CIfDataRepository extends JpaRepository<CIfData, Long> {}
