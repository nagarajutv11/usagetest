package repository.jpa;

import models.ModelTestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelTestDataRepository extends JpaRepository<ModelTestData, Long> {}
