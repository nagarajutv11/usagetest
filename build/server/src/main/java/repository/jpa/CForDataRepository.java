package repository.jpa;

import models.CForData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CForDataRepository extends JpaRepository<CForData, Long> {}
