package store;

public interface EntityValidationContext {

	boolean hasErrors();

	Object getErrors();

	void addFieldError(String field, String error);

	void addEntityError(String error);
	
	EntityValidationContext child(String field, long index);

}
