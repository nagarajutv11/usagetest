package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.CNodeDataEntityInput;
import graphql.input.CRefSlotDataEntityInput;
import models.CNodeData;
import models.CRefSlotData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CRefDataRepository;
import repository.jpa.CRefSlotDataRepository;
import repository.jpa.CSlotDataRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CRefSlotData")
public class CRefSlotDataEntityHelper<T extends CRefSlotData, I extends CRefSlotDataEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private CRefSlotDataRepository cRefSlotDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CRefDataRepository cRefDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CSlotDataRepository cSlotDataRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public CRefSlotData newInstance() {
    return new CRefSlotData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCRefSlotData = ((T) new CRefSlotData());
    newCRefSlotData.setId(input.getId());
    return fromInput(input, newCRefSlotData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("slot")) {
      entity.setSlot(input.getSlot());
    }
    if (helper.has("child")) {
      if (input.getChild() != null) {
        CNodeDataEntityInput value = input.getChild().getValue();
        entity.setChild(((CNodeData) helper.readUnionChild(value, "child")));
      } else {
        entity.setChild(null);
      }
    }
    if (helper.has("children")) {
      entity.setChildren(
          IterableExt.toList(
              ListExt.map(
                  input.getChildren(),
                  (objId) -> {
                    CNodeDataEntityHelper childrenHelper = this.mutator.getHelper(objId._type());
                    return ((CNodeData) helper.readUnionChild(objId.getValue(), "children"));
                  })));
    }
    return entity;
  }

  public void validateFieldSlot(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getSlot();
      if (it == null) {
        validationContext.addFieldError("slot", "slot is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldSlot(entity, validationContext);
    long childIndex = 0l;
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.validate(entity.getChild(), validationContext.child("child", 0l));
    }
    long childrenIndex = 0l;
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("children", childrenIndex++));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cRefSlotDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.setDefaults(entity.getChild());
    }
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
  }

  @Override
  public void compute(T entity) {
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.compute(entity.getChild());
    }
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
