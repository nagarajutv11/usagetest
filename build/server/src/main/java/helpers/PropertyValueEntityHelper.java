package helpers;

import graphql.input.PropertyValueEntityInput;
import models.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CRefRepository;
import repository.jpa.PropertyValueRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("PropertyValue")
public class PropertyValueEntityHelper<T extends PropertyValue, I extends PropertyValueEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private PropertyValueRepository propertyValueRepository;
  @org.springframework.beans.factory.annotation.Autowired private CRefRepository cRefRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public PropertyValue newInstance() {
    return new PropertyValue();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newPropertyValue = ((T) new PropertyValue());
    newPropertyValue.setId(input.getId());
    return fromInput(input, newPropertyValue, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("thenValue")) {
      entity.setThenValue(input.getThenValue());
    }
    if (helper.has("elseValue")) {
      entity.setElseValue(input.getElseValue());
    }
    if (helper.has("defaultValue")) {
      entity.setDefaultValue(input.getDefaultValue());
    }
    if (helper.has("value")) {
      entity.setValue(input.getValue());
    }
    if (helper.has("fromTheme")) {
      entity.setFromTheme(input.getFromTheme());
    }
    return entity;
  }

  public void validateFieldValue(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getValue();
      if (it == null) {
        validationContext.addFieldError("value", "value is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldValue(entity, validationContext);
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) propertyValueRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {}

  @Override
  public void compute(T entity) {}

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
