package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.TestEntityInput;
import models.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.TestRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("Test")
public class TestEntityHelper<T extends Test, I extends TestEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private TestRepository testRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public Test newInstance() {
    return new Test();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newTest = ((T) new Test());
    newTest.setId(input.getId());
    return fromInput(input, newTest, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("childrens")) {
      entity.setChildrens(
          IterableExt.toList(
              ListExt.map(
                  input.getChildrens(),
                  (objId) -> {
                    return ((Test) helper.readRef(objId));
                  })));
    }
    entity.updateMasters((o) -> {});
    return entity;
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) testRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {}

  @Override
  public void compute(T entity) {}

  private void deleteChildrensInTest(T entity, EntityValidationContext deletionContext) {
    if (this.testRepository.findByChildrens(entity).size() > 0) {
      deletionContext.addEntityError("This cannot be deleted as it is being referred to by Test.");
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {
    this.deleteChildrensInTest(entity, deletionContext);
  }

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }

  public T getOld(long id) {
    return ((T) getById(id).clone());
  }
}
