package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.PropertyTestDataEntityInput;
import java.util.List;
import java.util.stream.Collectors;
import models.ComponentTestData;
import models.ModelTestData;
import models.PropertyTestData;
import models.StructTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CForDataRepository;
import repository.jpa.CRefDataRepository;
import repository.jpa.CSwitchDataRepository;
import repository.jpa.ComponentTestDataRepository;
import repository.jpa.ModelTestDataRepository;
import repository.jpa.PropertyTestDataRepository;
import repository.jpa.StructTestDataRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("PropertyTestData")
public class PropertyTestDataEntityHelper<
        T extends PropertyTestData, I extends PropertyTestDataEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private PropertyTestDataRepository propertyTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private ModelTestDataRepository modelTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private ComponentTestDataRepository componentTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private StructTestDataRepository structTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CForDataRepository cForDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CRefDataRepository cRefDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CSwitchDataRepository cSwitchDataRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public PropertyTestData newInstance() {
    return new PropertyTestData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newPropertyTestData = ((T) new PropertyTestData());
    newPropertyTestData.setId(input.getId());
    return fromInput(input, newPropertyTestData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("primitiveValue")) {
      entity.setPrimitiveValue(input.getPrimitiveValue().stream().collect(Collectors.toList()));
    }
    if (helper.has("modelValue")) {
      entity.setModelValue(
          IterableExt.toList(
              ListExt.map(
                  input.getModelValue(),
                  (objId) -> {
                    return ((ModelTestData) helper.readRef(objId));
                  })));
    }
    if (helper.has("structValue")) {
      entity.setStructValue(
          IterableExt.toList(
              ListExt.map(
                  input.getStructValue(),
                  (objId) -> {
                    return ((StructTestData) helper.readRef(objId));
                  })));
    }
    return entity;
  }

  public void validateFieldIdentity(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getIdentity();
      if (it == null) {
        validationContext.addFieldError("identity", "identity is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public List<StructTestData> computeStructValueReferenceFrom(T entity) {
    return entity.getIn() != null
        ? ListExt.toList(entity.getIn().getStructTempData(), false)
        : ListExt.asList();
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {
    for (StructTestData structValue : entity.getStructValue()) {
      if (structValue != null
          && !(IterableExt.contains(
              entity.getIn() != null
                  ? ListExt.toList(entity.getIn().getStructTempData(), false)
                  : ListExt.asList(),
              structValue))) {
        validationContext.addFieldError(
            "structValue", "structValue referenceFrom validation error");
        break;
      }
    }
  }

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldIdentity(entity, validationContext);
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) propertyTestDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {}

  @Override
  public void compute(T entity) {}

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
