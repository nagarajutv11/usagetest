package helpers;

import graphql.input.CSlotDataEntityInput;
import models.CRefSlotData;
import models.CSlotData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CSlotDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CSlotData")
public class CSlotDataEntityHelper<T extends CSlotData, I extends CSlotDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CSlotDataRepository cSlotDataRepository;

  public CSlotData newInstance() {
    return new CSlotData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCSlotData = ((T) new CSlotData());
    newCSlotData.setId(input.getId());
    return fromInput(input, newCSlotData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("slot")) {
      entity.setSlot(input.getSlot());
    }
    if (helper.has("actual")) {
      entity.setActual(((CRefSlotData) helper.readChild(input.getActual(), "actual")));
    }
    return entity;
  }

  public void validateFieldSlot(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getSlot();
      if (it == null) {
        validationContext.addFieldError("slot", "slot is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    validateFieldSlot(entity, validationContext);
    long actualIndex = 0l;
    if (entity.getActual() != null) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(entity.getActual());
      helper.validate(entity.getActual(), validationContext.child("actual", 0l));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cSlotDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    if (entity.getActual() != null) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(entity.getActual());
      helper.setDefaults(entity.getActual());
    }
  }

  @Override
  public void compute(T entity) {
    if (entity.getActual() != null) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(entity.getActual());
      helper.compute(entity.getActual());
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
