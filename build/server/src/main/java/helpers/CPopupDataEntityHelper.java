package helpers;

import graphql.input.CNodeDataEntityInput;
import graphql.input.CPopupDataEntityInput;
import models.CNodeData;
import models.CPopupData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CPopupDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CPopupData")
public class CPopupDataEntityHelper<T extends CPopupData, I extends CPopupDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CPopupDataRepository cPopupDataRepository;

  public CPopupData newInstance() {
    return new CPopupData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCPopupData = ((T) new CPopupData());
    newCPopupData.setId(input.getId());
    return fromInput(input, newCPopupData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("showOn")) {
      entity.setShowOn(input.isShowOn());
    }
    if (helper.has("body")) {
      if (input.getBody() != null) {
        CNodeDataEntityInput value = input.getBody().getValue();
        entity.setBody(((CNodeData) helper.readUnionChild(value, "body")));
      } else {
        entity.setBody(null);
      }
    }
    if (helper.has("placeHolder")) {
      if (input.getPlaceHolder() != null) {
        CNodeDataEntityInput value = input.getPlaceHolder().getValue();
        entity.setPlaceHolder(((CNodeData) helper.readUnionChild(value, "placeHolder")));
      } else {
        entity.setPlaceHolder(null);
      }
    }
    return entity;
  }

  public void validateFieldShowOn(T entity, EntityValidationContext validationContext) {
    try {
      boolean it = entity.isShowOn();
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    validateFieldShowOn(entity, validationContext);
    long bodyIndex = 0l;
    if (entity.getBody() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBody());
      helper.validate(entity.getBody(), validationContext.child("body", 0l));
    }
    long placeHolderIndex = 0l;
    if (entity.getPlaceHolder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getPlaceHolder());
      helper.validate(entity.getPlaceHolder(), validationContext.child("placeHolder", 0l));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cPopupDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    if (entity.getBody() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBody());
      helper.setDefaults(entity.getBody());
    }
    if (entity.getPlaceHolder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getPlaceHolder());
      helper.setDefaults(entity.getPlaceHolder());
    }
  }

  @Override
  public void compute(T entity) {
    if (entity.getBody() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBody());
      helper.compute(entity.getBody());
    }
    if (entity.getPlaceHolder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getPlaceHolder());
      helper.compute(entity.getPlaceHolder());
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
