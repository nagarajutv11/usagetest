package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.CNodeDataEntityInput;
import graphql.input.CRefDataEntityInput;
import models.CNodeData;
import models.CRefData;
import models.CRefSlotData;
import models.PropertyTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CRefDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CRefData")
public class CRefDataEntityHelper<T extends CRefData, I extends CRefDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CRefDataRepository cRefDataRepository;

  public CRefData newInstance() {
    return new CRefData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCRefData = ((T) new CRefData());
    newCRefData.setId(input.getId());
    return fromInput(input, newCRefData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("data")) {
      entity.setData(
          IterableExt.toList(
              ListExt.map(
                  input.getData(),
                  (objId) -> {
                    PropertyTestDataEntityHelper dataHelper = this.mutator.getHelper(objId._type());
                    return ((PropertyTestData) helper.readChild(objId, "data"));
                  })));
    }
    if (helper.has("builder")) {
      if (input.getBuilder() != null) {
        CNodeDataEntityInput value = input.getBuilder().getValue();
        entity.setBuilder(((CNodeData) helper.readUnionChild(value, "builder")));
      } else {
        entity.setBuilder(null);
      }
    }
    if (helper.has("buildData")) {
      if (input.getBuildData() != null) {
        CNodeDataEntityInput value = input.getBuildData().getValue();
        entity.setBuildData(((CNodeData) helper.readUnionChild(value, "buildData")));
      } else {
        entity.setBuildData(null);
      }
    }
    if (helper.has("child")) {
      if (input.getChild() != null) {
        CNodeDataEntityInput value = input.getChild().getValue();
        entity.setChild(((CNodeData) helper.readUnionChild(value, "child")));
      } else {
        entity.setChild(null);
      }
    }
    if (helper.has("children")) {
      entity.setChildren(
          IterableExt.toList(
              ListExt.map(
                  input.getChildren(),
                  (objId) -> {
                    CNodeDataEntityHelper childrenHelper = this.mutator.getHelper(objId._type());
                    return ((CNodeData) helper.readUnionChild(objId.getValue(), "children"));
                  })));
    }
    if (helper.has("slots")) {
      entity.setSlots(
          IterableExt.toList(
              ListExt.map(
                  input.getSlots(),
                  (objId) -> {
                    CRefSlotDataEntityHelper slotsHelper = this.mutator.getHelper(objId._type());
                    return ((CRefSlotData) helper.readChild(objId, "slots"));
                  })));
    }
    return entity;
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    long dataIndex = 0l;
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("data", dataIndex++));
    }
    long builderIndex = 0l;
    if (entity.getBuilder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuilder());
      helper.validate(entity.getBuilder(), validationContext.child("builder", 0l));
    }
    long buildDataIndex = 0l;
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.validate(entity.getBuildData(), validationContext.child("buildData", 0l));
    }
    long childIndex = 0l;
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.validate(entity.getChild(), validationContext.child("child", 0l));
    }
    long childrenIndex = 0l;
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("children", childrenIndex++));
    }
    long slotsIndex = 0l;
    for (CRefSlotData obj : entity.getSlots()) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("slots", slotsIndex++));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cRefDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
    if (entity.getBuilder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuilder());
      helper.setDefaults(entity.getBuilder());
    }
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.setDefaults(entity.getBuildData());
    }
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.setDefaults(entity.getChild());
    }
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
    for (CRefSlotData obj : entity.getSlots()) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
  }

  @Override
  public void compute(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
    if (entity.getBuilder() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuilder());
      helper.compute(entity.getBuilder());
    }
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.compute(entity.getBuildData());
    }
    if (entity.getChild() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getChild());
      helper.compute(entity.getChild());
    }
    for (CNodeData obj : entity.getChildren()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
    for (CRefSlotData obj : entity.getSlots()) {
      CRefSlotDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
