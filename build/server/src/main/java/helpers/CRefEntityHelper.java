package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.CRefEntityInput;
import models.CRef;
import models.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CRefRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CRef")
public class CRefEntityHelper<T extends CRef, I extends CRefEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private CRefRepository cRefRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public CRef newInstance() {
    return new CRef();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCRef = ((T) new CRef());
    newCRef.setId(input.getId());
    return fromInput(input, newCRef, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("data")) {
      entity.setData(
          IterableExt.toList(
              ListExt.map(
                  input.getData(),
                  (objId) -> {
                    PropertyValueEntityHelper dataHelper = this.mutator.getHelper(objId._type());
                    return ((PropertyValue) helper.readChild(objId, "data"));
                  })));
    }
    return entity;
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    long dataIndex = 0l;
    for (PropertyValue obj : entity.getData()) {
      PropertyValueEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("data", dataIndex++));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cRefRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    for (PropertyValue obj : entity.getData()) {
      PropertyValueEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
  }

  @Override
  public void compute(T entity) {
    for (PropertyValue obj : entity.getData()) {
      PropertyValueEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
