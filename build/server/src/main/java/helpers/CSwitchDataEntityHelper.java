package helpers;

import graphql.input.CNodeDataEntityInput;
import graphql.input.CSwitchDataEntityInput;
import models.CNodeData;
import models.CSwitchData;
import models.PropertyTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CSwitchDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CSwitchData")
public class CSwitchDataEntityHelper<T extends CSwitchData, I extends CSwitchDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CSwitchDataRepository cSwitchDataRepository;

  public CSwitchData newInstance() {
    return new CSwitchData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCSwitchData = ((T) new CSwitchData());
    newCSwitchData.setId(input.getId());
    return fromInput(input, newCSwitchData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("index")) {
      entity.setIndex(input.getIndex());
    }
    if (helper.has("onValue")) {
      entity.setOnValue(((PropertyTestData) helper.readChild(input.getOnValue(), "onValue")));
    }
    if (helper.has("caseData")) {
      if (input.getCaseData() != null) {
        CNodeDataEntityInput value = input.getCaseData().getValue();
        entity.setCaseData(((CNodeData) helper.readUnionChild(value, "caseData")));
      } else {
        entity.setCaseData(null);
      }
    }
    return entity;
  }

  public void validateFieldOnValue(T entity, EntityValidationContext validationContext) {
    try {
      PropertyTestData it = entity.getOnValue();
      if (it == null) {
        validationContext.addFieldError("onValue", "onValue is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    validateFieldOnValue(entity, validationContext);
    long onValueIndex = 0l;
    if (entity.getOnValue() != null) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(entity.getOnValue());
      helper.validate(entity.getOnValue(), validationContext.child("onValue", 0l));
    }
    long caseDataIndex = 0l;
    if (entity.getCaseData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getCaseData());
      helper.validate(entity.getCaseData(), validationContext.child("caseData", 0l));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cSwitchDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    if (entity.getOnValue() != null) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(entity.getOnValue());
      helper.setDefaults(entity.getOnValue());
    }
    if (entity.getCaseData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getCaseData());
      helper.setDefaults(entity.getCaseData());
    }
  }

  @Override
  public void compute(T entity) {
    if (entity.getOnValue() != null) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(entity.getOnValue());
      helper.compute(entity.getOnValue());
    }
    if (entity.getCaseData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getCaseData());
      helper.compute(entity.getCaseData());
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
