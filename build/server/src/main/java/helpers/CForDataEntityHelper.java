package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.CForDataEntityInput;
import models.CForData;
import models.CNodeData;
import models.PropertyTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CForDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CForData")
public class CForDataEntityHelper<T extends CForData, I extends CForDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CForDataRepository cForDataRepository;

  public CForData newInstance() {
    return new CForData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCForData = ((T) new CForData());
    newCForData.setId(input.getId());
    return fromInput(input, newCForData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("data")) {
      entity.setData(
          IterableExt.toList(
              ListExt.map(
                  input.getData(),
                  (objId) -> {
                    PropertyTestDataEntityHelper dataHelper = this.mutator.getHelper(objId._type());
                    return ((PropertyTestData) helper.readChild(objId, "data"));
                  })));
    }
    if (helper.has("items")) {
      entity.setItems(
          IterableExt.toList(
              ListExt.map(
                  input.getItems(),
                  (objId) -> {
                    CNodeDataEntityHelper itemsHelper = this.mutator.getHelper(objId._type());
                    return ((CNodeData) helper.readUnionChild(objId.getValue(), "items"));
                  })));
    }
    return entity;
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    long dataIndex = 0l;
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("data", dataIndex++));
    }
    long itemsIndex = 0l;
    for (CNodeData obj : entity.getItems()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("items", itemsIndex++));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cForDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
    for (CNodeData obj : entity.getItems()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
  }

  @Override
  public void compute(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
    for (CNodeData obj : entity.getItems()) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
