package helpers;

import graphql.input.CIfDataEntityInput;
import graphql.input.CNodeDataEntityInput;
import models.CIfData;
import models.CNodeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CIfDataRepository;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CIfData")
public class CIfDataEntityHelper<T extends CIfData, I extends CIfDataEntityInput>
    extends CNodeDataEntityHelper<T, I> {
  @Autowired private CIfDataRepository cIfDataRepository;

  public CIfData newInstance() {
    return new CIfData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newCIfData = ((T) new CIfData());
    newCIfData.setId(input.getId());
    return fromInput(input, newCIfData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    if (helper.has("condition")) {
      entity.setCondition(input.isCondition());
    }
    if (helper.has("value")) {
      if (input.getValue() != null) {
        CNodeDataEntityInput value = input.getValue().getValue();
        entity.setValue(((CNodeData) helper.readUnionChild(value, "value")));
      } else {
        entity.setValue(null);
      }
    }
    return entity;
  }

  public void validateFieldCondition(T entity, EntityValidationContext validationContext) {
    try {
      boolean it = entity.isCondition();
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    super.validate(entity, validationContext);
    referenceFromValidations(entity, validationContext);
    validateFieldCondition(entity, validationContext);
    long valueIndex = 0l;
    if (entity.getValue() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getValue());
      helper.validate(entity.getValue(), validationContext.child("value", 0l));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cIfDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    if (entity.getValue() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getValue());
      helper.setDefaults(entity.getValue());
    }
  }

  @Override
  public void compute(T entity) {
    if (entity.getValue() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getValue());
      helper.compute(entity.getValue());
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
