package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.ModelTestDataEntityInput;
import models.ModelTestData;
import models.PropertyTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.ModelTestDataRepository;
import repository.jpa.PropertyTestDataRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("ModelTestData")
public class ModelTestDataEntityHelper<T extends ModelTestData, I extends ModelTestDataEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private ModelTestDataRepository modelTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private PropertyTestDataRepository propertyTestDataRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public ModelTestData newInstance() {
    return new ModelTestData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newModelTestData = ((T) new ModelTestData());
    newModelTestData.setId(input.getId());
    return fromInput(input, newModelTestData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("data")) {
      entity.setData(
          IterableExt.toList(
              ListExt.map(
                  input.getData(),
                  (objId) -> {
                    PropertyTestDataEntityHelper dataHelper = this.mutator.getHelper(objId._type());
                    return ((PropertyTestData) helper.readChild(objId, "data"));
                  })));
    }
    entity.updateMasters((o) -> {});
    return entity;
  }

  public void validateFieldIdentity(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getIdentity();
      if (it == null) {
        validationContext.addFieldError("identity", "identity is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldIdentity(entity, validationContext);
    long dataIndex = 0l;
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("data", dataIndex++));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) modelTestDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
  }

  @Override
  public void compute(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
  }

  private void deleteModelValueInPropertyTestData(
      T entity, EntityValidationContext deletionContext) {
    if (this.propertyTestDataRepository.findByModelValue(entity).size() > 0) {
      deletionContext.addEntityError(
          "This cannot be deleted as it is being referred to by PropertyTestData.");
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {
    this.deleteModelValueInPropertyTestData(entity, deletionContext);
  }

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }

  public T getOld(long id) {
    return ((T) getById(id).clone());
  }
}
