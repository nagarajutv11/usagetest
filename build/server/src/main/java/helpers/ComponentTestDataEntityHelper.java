package helpers;

import d3e.core.IterableExt;
import d3e.core.ListExt;
import graphql.input.CNodeDataEntityInput;
import graphql.input.ComponentTestDataEntityInput;
import models.CNodeData;
import models.ComponentTestData;
import models.PropertyTestData;
import models.StructTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.ComponentTestDataRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("ComponentTestData")
public class ComponentTestDataEntityHelper<
        T extends ComponentTestData, I extends ComponentTestDataEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private ComponentTestDataRepository componentTestDataRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  public ComponentTestData newInstance() {
    return new ComponentTestData();
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    if (input == null) {
      return null;
    }
    T newComponentTestData = ((T) new ComponentTestData());
    newComponentTestData.setId(input.getId());
    return fromInput(input, newComponentTestData, helper);
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("data")) {
      entity.setData(
          IterableExt.toList(
              ListExt.map(
                  input.getData(),
                  (objId) -> {
                    PropertyTestDataEntityHelper dataHelper = this.mutator.getHelper(objId._type());
                    return ((PropertyTestData) helper.readChild(objId, "data"));
                  })));
    }
    if (helper.has("structTempData")) {
      entity.setStructTempData(
          IterableExt.toList(
              ListExt.map(
                  input.getStructTempData(),
                  (objId) -> {
                    StructTestDataEntityHelper structTempDataHelper =
                        this.mutator.getHelper(objId._type());
                    return ((StructTestData) helper.readChild(objId, "structTempData"));
                  })));
    }
    if (helper.has("buildData")) {
      if (input.getBuildData() != null) {
        CNodeDataEntityInput value = input.getBuildData().getValue();
        entity.setBuildData(((CNodeData) helper.readUnionChild(value, "buildData")));
      } else {
        entity.setBuildData(null);
      }
    }
    entity.updateMasters((o) -> {});
    return entity;
  }

  public void validateFieldIdentity(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getIdentity();
      if (it == null) {
        validationContext.addFieldError("identity", "identity is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldIdentity(entity, validationContext);
    long dataIndex = 0l;
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("data", dataIndex++));
    }
    long structTempDataIndex = 0l;
    for (StructTestData obj : entity.getStructTempData()) {
      StructTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.validate(obj, validationContext.child("structTempData", structTempDataIndex++));
    }
    long buildDataIndex = 0l;
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.validate(entity.getBuildData(), validationContext.child("buildData", 0l));
    }
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) componentTestDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
    for (StructTestData obj : entity.getStructTempData()) {
      StructTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.setDefaults(obj);
    }
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.setDefaults(entity.getBuildData());
    }
  }

  @Override
  public void compute(T entity) {
    for (PropertyTestData obj : entity.getData()) {
      PropertyTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
    for (StructTestData obj : entity.getStructTempData()) {
      StructTestDataEntityHelper helper = mutator.getHelperByInstance(obj);
      helper.compute(obj);
    }
    if (entity.getBuildData() != null) {
      CNodeDataEntityHelper helper = mutator.getHelperByInstance(entity.getBuildData());
      helper.compute(entity.getBuildData());
    }
  }

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }

  public T getOld(long id) {
    return ((T) getById(id).clone());
  }
}
