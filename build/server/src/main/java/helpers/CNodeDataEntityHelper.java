package helpers;

import graphql.input.CNodeDataEntityInput;
import models.CNodeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.jpa.CForDataRepository;
import repository.jpa.CIfDataRepository;
import repository.jpa.CNodeDataRepository;
import repository.jpa.CPopupDataRepository;
import repository.jpa.CRefDataRepository;
import repository.jpa.CRefSlotDataRepository;
import repository.jpa.CSwitchDataRepository;
import repository.jpa.ComponentTestDataRepository;
import store.EntityHelper;
import store.EntityMutator;
import store.EntityValidationContext;
import store.InputHelper;

@Service("CNodeData")
public class CNodeDataEntityHelper<T extends CNodeData, I extends CNodeDataEntityInput>
    implements EntityHelper<T, I> {
  @Autowired protected EntityMutator mutator;
  @Autowired private CNodeDataRepository cNodeDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private ComponentTestDataRepository componentTestDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CForDataRepository cForDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CIfDataRepository cIfDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CPopupDataRepository cPopupDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CRefDataRepository cRefDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CRefSlotDataRepository cRefSlotDataRepository;

  @org.springframework.beans.factory.annotation.Autowired
  private CSwitchDataRepository cSwitchDataRepository;

  public void setMutator(EntityMutator obj) {
    mutator = obj;
  }

  @Override
  public T fromInput(I input, InputHelper helper) {
    return null;
  }

  @Override
  public T fromInput(I input, T entity, InputHelper helper) {
    if (helper.has("identity")) {
      entity.setIdentity(input.getIdentity());
    }
    if (helper.has("x")) {
      entity.setX(input.getX());
    }
    if (helper.has("y")) {
      entity.setY(input.getY());
    }
    if (helper.has("width")) {
      entity.setWidth(input.getWidth());
    }
    if (helper.has("height")) {
      entity.setHeight(input.getHeight());
    }
    return entity;
  }

  public void validateFieldIdentity(T entity, EntityValidationContext validationContext) {
    try {
      String it = entity.getIdentity();
      if (it == null) {
        validationContext.addFieldError("identity", "identity is required.");
        return;
      }
    } catch (RuntimeException e) {
    }
  }

  public void referenceFromValidations(T entity, EntityValidationContext validationContext) {}

  public void validate(T entity, EntityValidationContext validationContext) {
    referenceFromValidations(entity, validationContext);
    validateFieldIdentity(entity, validationContext);
  }

  @Override
  public T clone(T entity) {
    return null;
  }

  @Override
  public T getById(long id) {
    return id == 0l ? null : ((T) cNodeDataRepository.getOne(id));
  }

  @Override
  public void setDefaults(T entity) {}

  @Override
  public void compute(T entity) {}

  public Boolean onDelete(T entity, EntityValidationContext deletionContext) {
    return true;
  }

  public void validateOnDelete(T entity, EntityValidationContext deletionContext) {}

  @Override
  public Boolean onCreate(T entity) {
    return true;
  }

  @Override
  public Boolean onUpdate(T entity, T old) {
    return true;
  }
}
