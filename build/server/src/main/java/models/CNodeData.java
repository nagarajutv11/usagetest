package models;

import java.util.function.Consumer;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public abstract class CNodeData extends DatabaseObject {
  @Field @Indexed @NotNull private String identity;
  @Field @Indexed private double x = 0.0d;
  @Field @Indexed private double y = 0.0d;
  @Field @Indexed private double width = 0.0d;
  @Field @Indexed private double height = 0.0d;
  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CIfData masterCIfData;
  @Field @ManyToOne private CPopupData masterCPopupData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CRefSlotData masterCRefSlotData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;
  private transient ComponentTestData in;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCIfData) {
      return masterCIfData;
    }
    if (null != masterCPopupData) {
      return masterCPopupData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCRefSlotData) {
      return masterCRefSlotData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCIfData != null) {
      masterCIfData.updateFlat(obj);
    }
    if (masterCPopupData != null) {
      masterCPopupData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCRefSlotData != null) {
      masterCRefSlotData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public double getX() {
    return this.x;
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getY() {
    return this.y;
  }

  public void setY(double y) {
    this.y = y;
  }

  public double getWidth() {
    return this.width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getHeight() {
    return this.height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CIfData getMasterCIfData() {
    return this.masterCIfData;
  }

  public void setMasterCIfData(CIfData masterCIfData) {
    this.masterCIfData = masterCIfData;
  }

  public CPopupData getMasterCPopupData() {
    return this.masterCPopupData;
  }

  public void setMasterCPopupData(CPopupData masterCPopupData) {
    this.masterCPopupData = masterCPopupData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CRefSlotData getMasterCRefSlotData() {
    return this.masterCRefSlotData;
  }

  public void setMasterCRefSlotData(CRefSlotData masterCRefSlotData) {
    this.masterCRefSlotData = masterCRefSlotData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public ComponentTestData getIn() {
    return this.in;
  }

  public void setIn(ComponentTestData in) {
    this.in = in;
  }

  public String displayName() {
    return "CNodeData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CNodeData && super.equals(a);
  }

  public CNodeData cloneInstance(CNodeData cloneObj) {
    super.cloneInstance(cloneObj);
    cloneObj.setIdentity(this.getIdentity());
    cloneObj.setX(this.getX());
    cloneObj.setY(this.getY());
    cloneObj.setWidth(this.getWidth());
    cloneObj.setHeight(this.getHeight());
    return cloneObj;
  }
}
