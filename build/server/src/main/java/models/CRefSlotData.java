package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CRefSlotData extends DatabaseObject {
  @Field @Indexed @NotNull private String slot;
  @Field @Indexed @ChildDocument @ManyToOne private CNodeData child;
  @Field @Indexed @ChildDocument @OneToMany private List<CNodeData> children = new ArrayList<>();
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CSlotData masterCSlotData;
  private transient ComponentTestData in;

  public Object _masterObject() {
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCSlotData) {
      return masterCSlotData;
    }
    return null;
  }

  public void addToChildren(CNodeData val, long index) {
    val.setMasterCRefSlotData(this);
    if (index == -1) {
      this.children.add(val);
    } else {
      this.children.add(((int) index), val);
    }
  }

  public void removeFromChildren(CNodeData val) {
    this.children.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    if (child != null) {
      visitor.accept(child);
      child.setMasterCRefSlotData(this);
      child.setIn(this.getIn());
      child.updateMasters(visitor);
    }
    for (CNodeData obj : this.getChildren()) {
      visitor.accept(obj);
      obj.setMasterCRefSlotData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCSlotData != null) {
      masterCSlotData.updateFlat(obj);
    }
  }

  public String getSlot() {
    return this.slot;
  }

  public void setSlot(String slot) {
    this.slot = slot;
  }

  public CNodeData getChild() {
    return this.child;
  }

  public void setChild(CNodeData child) {
    this.child = child;
  }

  public List<CNodeData> getChildren() {
    return this.children;
  }

  public void setChildren(List<CNodeData> children) {
    this.children.clear();
    this.children.addAll(children);
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CSlotData getMasterCSlotData() {
    return this.masterCSlotData;
  }

  public void setMasterCSlotData(CSlotData masterCSlotData) {
    this.masterCSlotData = masterCSlotData;
  }

  public ComponentTestData getIn() {
    return this.in;
  }

  public void setIn(ComponentTestData in) {
    this.in = in;
  }

  public String displayName() {
    return "CRefSlotData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CRefSlotData && super.equals(a);
  }

  public CRefSlotData cloneInstance(CRefSlotData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CRefSlotData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setSlot(this.getSlot());
    cloneObj.setChild(this.getChild() == null ? null : this.getChild().cloneInstance(null));
    cloneObj.setChildren(
        this.getChildren().stream()
            .map((CNodeData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
