package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class PropertyTestData extends DatabaseObject {
  @Field @Indexed @NotNull private String identity;
  @Field @Indexed @ElementCollection private List<String> primitiveValue = new ArrayList<>();
  @Field @Indexed @ManyToMany private List<ModelTestData> modelValue = new ArrayList<>();
  @Field @Indexed @ManyToMany private List<StructTestData> structValue = new ArrayList<>();
  @Field @ManyToOne private ModelTestData masterModelTestData;
  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private StructTestData masterStructTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;
  private transient ComponentTestData in;

  public Object _masterObject() {
    if (null != masterModelTestData) {
      return masterModelTestData;
    }
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterStructTestData) {
      return masterStructTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void addToPrimitiveValue(String val, long index) {
    if (index == -1) {
      this.primitiveValue.add(val);
    } else {
      this.primitiveValue.add(((int) index), val);
    }
  }

  public void removeFromPrimitiveValue(String val) {
    this.primitiveValue.remove(val);
  }

  public void addToModelValue(ModelTestData val, long index) {
    if (index == -1) {
      this.modelValue.add(val);
    } else {
      this.modelValue.add(((int) index), val);
    }
  }

  public void removeFromModelValue(ModelTestData val) {
    this.modelValue.remove(val);
  }

  public void addToStructValue(StructTestData val, long index) {
    if (index == -1) {
      this.structValue.add(val);
    } else {
      this.structValue.add(((int) index), val);
    }
  }

  public void removeFromStructValue(StructTestData val) {
    this.structValue.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterModelTestData != null) {
      masterModelTestData.updateFlat(obj);
    }
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterStructTestData != null) {
      masterStructTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<String> getPrimitiveValue() {
    return this.primitiveValue;
  }

  public void setPrimitiveValue(List<String> primitiveValue) {
    this.primitiveValue.clear();
    this.primitiveValue.addAll(primitiveValue);
  }

  public List<ModelTestData> getModelValue() {
    return this.modelValue;
  }

  public void setModelValue(List<ModelTestData> modelValue) {
    this.modelValue.clear();
    this.modelValue.addAll(modelValue);
  }

  public List<StructTestData> getStructValue() {
    return this.structValue;
  }

  public void setStructValue(List<StructTestData> structValue) {
    this.structValue.clear();
    this.structValue.addAll(structValue);
  }

  public ModelTestData getMasterModelTestData() {
    return this.masterModelTestData;
  }

  public void setMasterModelTestData(ModelTestData masterModelTestData) {
    this.masterModelTestData = masterModelTestData;
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public StructTestData getMasterStructTestData() {
    return this.masterStructTestData;
  }

  public void setMasterStructTestData(StructTestData masterStructTestData) {
    this.masterStructTestData = masterStructTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public ComponentTestData getIn() {
    return this.in;
  }

  public void setIn(ComponentTestData in) {
    this.in = in;
  }

  public String displayName() {
    return "PropertyTestData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof PropertyTestData && super.equals(a);
  }

  public PropertyTestData cloneInstance(PropertyTestData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new PropertyTestData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setIdentity(this.getIdentity());
    cloneObj.setPrimitiveValue(new ArrayList<>(this.getPrimitiveValue()));
    cloneObj.setModelValue(new ArrayList<>(this.getModelValue()));
    cloneObj.setStructValue(new ArrayList<>(this.getStructValue()));
    return cloneObj;
  }
}
