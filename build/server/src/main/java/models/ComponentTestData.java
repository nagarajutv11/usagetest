package models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import store.DatabaseObject;

@SolrDocument(collection = "ComponentTestData")
@Entity
public class ComponentTestData extends CreatableObject {
  @Field @Indexed @NotNull private String identity;

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyTestData> data = new ArrayList<>();

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<StructTestData> structTempData = new ArrayList<>();

  @Field @Indexed @ChildDocument @ManyToOne private CNodeData buildData;

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<CNodeData> flatBuildData = new HashSet<>();

  private transient ComponentTestData old;

  public void addToData(PropertyTestData val, long index) {
    val.setMasterComponentTestData(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyTestData val) {
    this.data.remove(val);
  }

  public void addToStructTempData(StructTestData val, long index) {
    val.setMasterComponentTestData(this);
    if (index == -1) {
      this.structTempData.add(val);
    } else {
      this.structTempData.add(((int) index), val);
    }
  }

  public void removeFromStructTempData(StructTestData val) {
    this.structTempData.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyTestData obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterComponentTestData(this);
      obj.setIn(this);
      obj.updateMasters(visitor);
    }
    for (StructTestData obj : this.getStructTempData()) {
      visitor.accept(obj);
      obj.setMasterComponentTestData(this);
      obj.setIn(this);
      obj.updateMasters(visitor);
    }
    if (buildData != null) {
      visitor.accept(buildData);
      buildData.setMasterComponentTestData(this);
      buildData.setIn(this);
      buildData.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (obj instanceof CNodeData) {
      flatBuildData.add(((CNodeData) obj));
    }
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<PropertyTestData> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestData> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public List<StructTestData> getStructTempData() {
    return this.structTempData;
  }

  public void setStructTempData(List<StructTestData> structTempData) {
    this.structTempData.clear();
    this.structTempData.addAll(structTempData);
  }

  public CNodeData getBuildData() {
    return this.buildData;
  }

  public Set<CNodeData> getFlatBuildData() {
    return this.flatBuildData;
  }

  public void setBuildData(CNodeData buildData) {
    this.buildData = buildData;
  }

  public ComponentTestData getOld() {
    return this.old;
  }

  public void setOld(ComponentTestData old) {
    this.old = old;
  }

  public String displayName() {
    return "ComponentTestData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof ComponentTestData && super.equals(a);
  }

  public ComponentTestData cloneInstance(ComponentTestData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new ComponentTestData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setIdentity(this.getIdentity());
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    cloneObj.setStructTempData(
        this.getStructTempData().stream()
            .map((StructTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    cloneObj.setBuildData(
        this.getBuildData() == null ? null : this.getBuildData().cloneInstance(null));
    return cloneObj;
  }
}
