package models;

import java.util.function.Consumer;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class PropertyValue extends DatabaseObject {
  @Field @Indexed private String thenValue;
  @Field @Indexed private String elseValue;
  @Field @Indexed private String defaultValue;
  @Field @Indexed @NotNull private String value;
  @Field @Indexed private String fromTheme;
  @Field @ManyToOne private CRef masterCRef;

  public Object _masterObject() {
    if (null != masterCRef) {
      return masterCRef;
    }
    return null;
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterCRef != null) {
      masterCRef.updateFlat(obj);
    }
  }

  public String getThenValue() {
    return this.thenValue;
  }

  public void setThenValue(String thenValue) {
    this.thenValue = thenValue;
  }

  public String getElseValue() {
    return this.elseValue;
  }

  public void setElseValue(String elseValue) {
    this.elseValue = elseValue;
  }

  public String getDefaultValue() {
    return this.defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getFromTheme() {
    return this.fromTheme;
  }

  public void setFromTheme(String fromTheme) {
    this.fromTheme = fromTheme;
  }

  public CRef getMasterCRef() {
    return this.masterCRef;
  }

  public void setMasterCRef(CRef masterCRef) {
    this.masterCRef = masterCRef;
  }

  public String displayName() {
    return "PropertyValue";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof PropertyValue && super.equals(a);
  }

  public PropertyValue cloneInstance(PropertyValue cloneObj) {
    if (cloneObj == null) {
      cloneObj = new PropertyValue();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setThenValue(this.getThenValue());
    cloneObj.setElseValue(this.getElseValue());
    cloneObj.setDefaultValue(this.getDefaultValue());
    cloneObj.setValue(this.getValue());
    cloneObj.setFromTheme(this.getFromTheme());
    return cloneObj;
  }
}
