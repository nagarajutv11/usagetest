package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import store.DatabaseObject;

@SolrDocument(collection = "ModelTestData")
@Entity
public class ModelTestData extends CreatableObject {
  @Field @Indexed @NotNull private String identity;

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyTestData> data = new ArrayList<>();

  private transient ModelTestData old;

  public void addToData(PropertyTestData val, long index) {
    val.setMasterModelTestData(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyTestData val) {
    this.data.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyTestData obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterModelTestData(this);
      obj.setIn(null);
      obj.updateMasters(visitor);
    }
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<PropertyTestData> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestData> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public ModelTestData getOld() {
    return this.old;
  }

  public void setOld(ModelTestData old) {
    this.old = old;
  }

  public String displayName() {
    return "ModelTestData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof ModelTestData && super.equals(a);
  }

  public ModelTestData cloneInstance(ModelTestData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new ModelTestData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setIdentity(this.getIdentity());
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
