package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CRefData extends CNodeData {
  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyTestData> data = new ArrayList<>();

  @Field @Indexed @ChildDocument @ManyToOne private CNodeData builder;
  @Field @Indexed @ChildDocument @ManyToOne private CNodeData buildData;
  @Field @Indexed @ChildDocument @ManyToOne private CNodeData child;
  @Field @Indexed @ChildDocument @OneToMany private List<CNodeData> children = new ArrayList<>();

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<CRefSlotData> slots = new ArrayList<>();

  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CIfData masterCIfData;
  @Field @ManyToOne private CPopupData masterCPopupData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CRefSlotData masterCRefSlotData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCIfData) {
      return masterCIfData;
    }
    if (null != masterCPopupData) {
      return masterCPopupData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCRefSlotData) {
      return masterCRefSlotData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void addToData(PropertyTestData val, long index) {
    val.setMasterCRefData(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyTestData val) {
    this.data.remove(val);
  }

  public void addToChildren(CNodeData val, long index) {
    val.setMasterCRefData(this);
    if (index == -1) {
      this.children.add(val);
    } else {
      this.children.add(((int) index), val);
    }
  }

  public void removeFromChildren(CNodeData val) {
    this.children.remove(val);
  }

  public void addToSlots(CRefSlotData val, long index) {
    val.setMasterCRefData(this);
    if (index == -1) {
      this.slots.add(val);
    } else {
      this.slots.add(((int) index), val);
    }
  }

  public void removeFromSlots(CRefSlotData val) {
    this.slots.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyTestData obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterCRefData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
    if (builder != null) {
      visitor.accept(builder);
      builder.setMasterCRefData(this);
      builder.setIn(this.getIn());
      builder.updateMasters(visitor);
    }
    if (buildData != null) {
      visitor.accept(buildData);
      buildData.setMasterCRefData(this);
      buildData.setIn(this.getIn());
      buildData.updateMasters(visitor);
    }
    if (child != null) {
      visitor.accept(child);
      child.setMasterCRefData(this);
      child.setIn(this.getIn());
      child.updateMasters(visitor);
    }
    for (CNodeData obj : this.getChildren()) {
      visitor.accept(obj);
      obj.setMasterCRefData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
    for (CRefSlotData obj : this.getSlots()) {
      visitor.accept(obj);
      obj.setMasterCRefData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCIfData != null) {
      masterCIfData.updateFlat(obj);
    }
    if (masterCPopupData != null) {
      masterCPopupData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCRefSlotData != null) {
      masterCRefSlotData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public List<PropertyTestData> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestData> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public CNodeData getBuilder() {
    return this.builder;
  }

  public void setBuilder(CNodeData builder) {
    this.builder = builder;
  }

  public CNodeData getBuildData() {
    return this.buildData;
  }

  public void setBuildData(CNodeData buildData) {
    this.buildData = buildData;
  }

  public CNodeData getChild() {
    return this.child;
  }

  public void setChild(CNodeData child) {
    this.child = child;
  }

  public List<CNodeData> getChildren() {
    return this.children;
  }

  public void setChildren(List<CNodeData> children) {
    this.children.clear();
    this.children.addAll(children);
  }

  public List<CRefSlotData> getSlots() {
    return this.slots;
  }

  public void setSlots(List<CRefSlotData> slots) {
    this.slots.clear();
    this.slots.addAll(slots);
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CIfData getMasterCIfData() {
    return this.masterCIfData;
  }

  public void setMasterCIfData(CIfData masterCIfData) {
    this.masterCIfData = masterCIfData;
  }

  public CPopupData getMasterCPopupData() {
    return this.masterCPopupData;
  }

  public void setMasterCPopupData(CPopupData masterCPopupData) {
    this.masterCPopupData = masterCPopupData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CRefSlotData getMasterCRefSlotData() {
    return this.masterCRefSlotData;
  }

  public void setMasterCRefSlotData(CRefSlotData masterCRefSlotData) {
    this.masterCRefSlotData = masterCRefSlotData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public String displayName() {
    return "CRefData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CRefData && super.equals(a);
  }

  public CRefData cloneInstance(CRefData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CRefData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    cloneObj.setBuilder(this.getBuilder() == null ? null : this.getBuilder().cloneInstance(null));
    cloneObj.setBuildData(
        this.getBuildData() == null ? null : this.getBuildData().cloneInstance(null));
    cloneObj.setChild(this.getChild() == null ? null : this.getChild().cloneInstance(null));
    cloneObj.setChildren(
        this.getChildren().stream()
            .map((CNodeData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    cloneObj.setSlots(
        this.getSlots().stream()
            .map((CRefSlotData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
