package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import store.DatabaseObject;

@SolrDocument(collection = "Test")
@Entity
public class Test extends CreatableObject {
  @Field @Indexed @ManyToMany private List<Test> childrens = new ArrayList<>();
  private transient Test old;

  public void addToChildrens(Test val, long index) {
    if (index == -1) {
      this.childrens.add(val);
    } else {
      this.childrens.add(((int) index), val);
    }
  }

  public void removeFromChildrens(Test val) {
    this.childrens.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
  }

  public List<Test> getChildrens() {
    return this.childrens;
  }

  public void setChildrens(List<Test> childrens) {
    this.childrens.clear();
    this.childrens.addAll(childrens);
  }

  public Test getOld() {
    return this.old;
  }

  public void setOld(Test old) {
    this.old = old;
  }

  public String displayName() {
    return "Test";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof Test && super.equals(a);
  }

  public Test cloneInstance(Test cloneObj) {
    if (cloneObj == null) {
      cloneObj = new Test();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setChildrens(new ArrayList<>(this.getChildrens()));
    return cloneObj;
  }
}
