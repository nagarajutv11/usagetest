package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class StructTestData extends DatabaseObject {
  @Field @Indexed @NotNull private String identity;

  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyTestData> data = new ArrayList<>();

  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  private transient ComponentTestData in;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    return null;
  }

  public void addToData(PropertyTestData val, long index) {
    val.setMasterStructTestData(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyTestData val) {
    this.data.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyTestData obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterStructTestData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
  }

  public String getIdentity() {
    return this.identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public List<PropertyTestData> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestData> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public ComponentTestData getIn() {
    return this.in;
  }

  public void setIn(ComponentTestData in) {
    this.in = in;
  }

  public String displayName() {
    return "StructTestData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof StructTestData && super.equals(a);
  }

  public StructTestData cloneInstance(StructTestData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new StructTestData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setIdentity(this.getIdentity());
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
