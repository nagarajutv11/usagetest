package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CRef extends DatabaseObject {
  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyValue> data = new ArrayList<>();

  public void addToData(PropertyValue val, long index) {
    val.setMasterCRef(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyValue val) {
    this.data.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyValue obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterCRef(this);
      obj.updateMasters(visitor);
    }
  }

  public List<PropertyValue> getData() {
    return this.data;
  }

  public void setData(List<PropertyValue> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public String displayName() {
    return "CRef";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CRef && super.equals(a);
  }

  public CRef cloneInstance(CRef cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CRef();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyValue colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
