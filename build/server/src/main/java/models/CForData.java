package models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CForData extends CNodeData {
  @Field
  @Indexed
  @ChildDocument
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PropertyTestData> data = new ArrayList<>();

  @Field @Indexed @ChildDocument @OneToMany private List<CNodeData> items = new ArrayList<>();
  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CIfData masterCIfData;
  @Field @ManyToOne private CPopupData masterCPopupData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CRefSlotData masterCRefSlotData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCIfData) {
      return masterCIfData;
    }
    if (null != masterCPopupData) {
      return masterCPopupData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCRefSlotData) {
      return masterCRefSlotData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void addToData(PropertyTestData val, long index) {
    val.setMasterCForData(this);
    if (index == -1) {
      this.data.add(val);
    } else {
      this.data.add(((int) index), val);
    }
  }

  public void removeFromData(PropertyTestData val) {
    this.data.remove(val);
  }

  public void addToItems(CNodeData val, long index) {
    val.setMasterCForData(this);
    if (index == -1) {
      this.items.add(val);
    } else {
      this.items.add(((int) index), val);
    }
  }

  public void removeFromItems(CNodeData val) {
    this.items.remove(val);
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    for (PropertyTestData obj : this.getData()) {
      visitor.accept(obj);
      obj.setMasterCForData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
    for (CNodeData obj : this.getItems()) {
      visitor.accept(obj);
      obj.setMasterCForData(this);
      obj.setIn(this.getIn());
      obj.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCIfData != null) {
      masterCIfData.updateFlat(obj);
    }
    if (masterCPopupData != null) {
      masterCPopupData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCRefSlotData != null) {
      masterCRefSlotData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public List<PropertyTestData> getData() {
    return this.data;
  }

  public void setData(List<PropertyTestData> data) {
    this.data.clear();
    this.data.addAll(data);
  }

  public List<CNodeData> getItems() {
    return this.items;
  }

  public void setItems(List<CNodeData> items) {
    this.items.clear();
    this.items.addAll(items);
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CIfData getMasterCIfData() {
    return this.masterCIfData;
  }

  public void setMasterCIfData(CIfData masterCIfData) {
    this.masterCIfData = masterCIfData;
  }

  public CPopupData getMasterCPopupData() {
    return this.masterCPopupData;
  }

  public void setMasterCPopupData(CPopupData masterCPopupData) {
    this.masterCPopupData = masterCPopupData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CRefSlotData getMasterCRefSlotData() {
    return this.masterCRefSlotData;
  }

  public void setMasterCRefSlotData(CRefSlotData masterCRefSlotData) {
    this.masterCRefSlotData = masterCRefSlotData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public String displayName() {
    return "CForData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CForData && super.equals(a);
  }

  public CForData cloneInstance(CForData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CForData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setData(
        this.getData().stream()
            .map((PropertyTestData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    cloneObj.setItems(
        this.getItems().stream()
            .map((CNodeData colObj) -> colObj.cloneInstance(null))
            .collect(Collectors.toList()));
    return cloneObj;
  }
}
