package models;

import java.util.function.Consumer;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CSwitchData extends CNodeData {
  @Field @Indexed private long index = 0l;

  @Field
  @Indexed
  @ChildDocument
  @NotNull
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  private PropertyTestData onValue;

  @Field @Indexed @ChildDocument @ManyToOne private CNodeData caseData;
  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CIfData masterCIfData;
  @Field @ManyToOne private CPopupData masterCPopupData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CRefSlotData masterCRefSlotData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCIfData) {
      return masterCIfData;
    }
    if (null != masterCPopupData) {
      return masterCPopupData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCRefSlotData) {
      return masterCRefSlotData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    if (onValue != null) {
      visitor.accept(onValue);
      onValue.setMasterCSwitchData(this);
      onValue.setIn(this.getIn());
      onValue.updateMasters(visitor);
    }
    if (caseData != null) {
      visitor.accept(caseData);
      caseData.setMasterCSwitchData(this);
      caseData.setIn(this.getIn());
      caseData.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCIfData != null) {
      masterCIfData.updateFlat(obj);
    }
    if (masterCPopupData != null) {
      masterCPopupData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCRefSlotData != null) {
      masterCRefSlotData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public long getIndex() {
    return this.index;
  }

  public void setIndex(long index) {
    this.index = index;
  }

  public PropertyTestData getOnValue() {
    return this.onValue;
  }

  public void setOnValue(PropertyTestData onValue) {
    this.onValue = onValue;
  }

  public CNodeData getCaseData() {
    return this.caseData;
  }

  public void setCaseData(CNodeData caseData) {
    this.caseData = caseData;
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CIfData getMasterCIfData() {
    return this.masterCIfData;
  }

  public void setMasterCIfData(CIfData masterCIfData) {
    this.masterCIfData = masterCIfData;
  }

  public CPopupData getMasterCPopupData() {
    return this.masterCPopupData;
  }

  public void setMasterCPopupData(CPopupData masterCPopupData) {
    this.masterCPopupData = masterCPopupData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CRefSlotData getMasterCRefSlotData() {
    return this.masterCRefSlotData;
  }

  public void setMasterCRefSlotData(CRefSlotData masterCRefSlotData) {
    this.masterCRefSlotData = masterCRefSlotData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public String displayName() {
    return "CSwitchData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CSwitchData && super.equals(a);
  }

  public CSwitchData cloneInstance(CSwitchData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CSwitchData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setIndex(this.getIndex());
    cloneObj.setOnValue(this.getOnValue() == null ? null : this.getOnValue().cloneInstance(null));
    cloneObj.setCaseData(
        this.getCaseData() == null ? null : this.getCaseData().cloneInstance(null));
    return cloneObj;
  }
}
