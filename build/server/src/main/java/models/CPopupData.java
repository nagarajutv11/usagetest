package models;

import java.util.function.Consumer;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.ChildDocument;
import org.springframework.data.solr.core.mapping.Indexed;
import store.DatabaseObject;

@Entity
public class CPopupData extends CNodeData {
  @Field @Indexed @NotNull private boolean showOn = false;
  @Field @Indexed @ChildDocument @ManyToOne private CNodeData body;
  @Field @Indexed @ChildDocument @ManyToOne private CNodeData placeHolder;
  @Field @ManyToOne private ComponentTestData masterComponentTestData;
  @Field @ManyToOne private CForData masterCForData;
  @Field @ManyToOne private CIfData masterCIfData;
  @Field @ManyToOne private CPopupData masterCPopupData;
  @Field @ManyToOne private CRefData masterCRefData;
  @Field @ManyToOne private CRefSlotData masterCRefSlotData;
  @Field @ManyToOne private CSwitchData masterCSwitchData;

  public Object _masterObject() {
    if (null != masterComponentTestData) {
      return masterComponentTestData;
    }
    if (null != masterCForData) {
      return masterCForData;
    }
    if (null != masterCIfData) {
      return masterCIfData;
    }
    if (null != masterCPopupData) {
      return masterCPopupData;
    }
    if (null != masterCRefData) {
      return masterCRefData;
    }
    if (null != masterCRefSlotData) {
      return masterCRefSlotData;
    }
    if (null != masterCSwitchData) {
      return masterCSwitchData;
    }
    return null;
  }

  public void updateMasters(Consumer<DatabaseObject> visitor) {
    super.updateMasters(visitor);
    if (body != null) {
      visitor.accept(body);
      body.setMasterCPopupData(this);
      body.setIn(this.getIn());
      body.updateMasters(visitor);
    }
    if (placeHolder != null) {
      visitor.accept(placeHolder);
      placeHolder.setMasterCPopupData(this);
      placeHolder.setIn(this.getIn());
      placeHolder.updateMasters(visitor);
    }
  }

  public void updateFlat(DatabaseObject obj) {
    super.updateFlat(obj);
    if (masterComponentTestData != null) {
      masterComponentTestData.updateFlat(obj);
    }
    if (masterCForData != null) {
      masterCForData.updateFlat(obj);
    }
    if (masterCIfData != null) {
      masterCIfData.updateFlat(obj);
    }
    if (masterCPopupData != null) {
      masterCPopupData.updateFlat(obj);
    }
    if (masterCRefData != null) {
      masterCRefData.updateFlat(obj);
    }
    if (masterCRefSlotData != null) {
      masterCRefSlotData.updateFlat(obj);
    }
    if (masterCSwitchData != null) {
      masterCSwitchData.updateFlat(obj);
    }
  }

  public boolean isShowOn() {
    return this.showOn;
  }

  public void setShowOn(boolean showOn) {
    this.showOn = showOn;
  }

  public CNodeData getBody() {
    return this.body;
  }

  public void setBody(CNodeData body) {
    this.body = body;
  }

  public CNodeData getPlaceHolder() {
    return this.placeHolder;
  }

  public void setPlaceHolder(CNodeData placeHolder) {
    this.placeHolder = placeHolder;
  }

  public ComponentTestData getMasterComponentTestData() {
    return this.masterComponentTestData;
  }

  public void setMasterComponentTestData(ComponentTestData masterComponentTestData) {
    this.masterComponentTestData = masterComponentTestData;
  }

  public CForData getMasterCForData() {
    return this.masterCForData;
  }

  public void setMasterCForData(CForData masterCForData) {
    this.masterCForData = masterCForData;
  }

  public CIfData getMasterCIfData() {
    return this.masterCIfData;
  }

  public void setMasterCIfData(CIfData masterCIfData) {
    this.masterCIfData = masterCIfData;
  }

  public CPopupData getMasterCPopupData() {
    return this.masterCPopupData;
  }

  public void setMasterCPopupData(CPopupData masterCPopupData) {
    this.masterCPopupData = masterCPopupData;
  }

  public CRefData getMasterCRefData() {
    return this.masterCRefData;
  }

  public void setMasterCRefData(CRefData masterCRefData) {
    this.masterCRefData = masterCRefData;
  }

  public CRefSlotData getMasterCRefSlotData() {
    return this.masterCRefSlotData;
  }

  public void setMasterCRefSlotData(CRefSlotData masterCRefSlotData) {
    this.masterCRefSlotData = masterCRefSlotData;
  }

  public CSwitchData getMasterCSwitchData() {
    return this.masterCSwitchData;
  }

  public void setMasterCSwitchData(CSwitchData masterCSwitchData) {
    this.masterCSwitchData = masterCSwitchData;
  }

  public String displayName() {
    return "CPopupData";
  }

  @Override
  public boolean equals(Object a) {
    return a instanceof CPopupData && super.equals(a);
  }

  public CPopupData cloneInstance(CPopupData cloneObj) {
    if (cloneObj == null) {
      cloneObj = new CPopupData();
    }
    super.cloneInstance(cloneObj);
    cloneObj.setShowOn(this.isShowOn());
    cloneObj.setBody(this.getBody() == null ? null : this.getBody().cloneInstance(null));
    cloneObj.setPlaceHolder(
        this.getPlaceHolder() == null ? null : this.getPlaceHolder().cloneInstance(null));
    return cloneObj;
  }
}
